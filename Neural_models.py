"""**Imports**"""

import numpy as np
import pandas as pd
from plotly.offline import plot
import plotly.express as px
import os
import keras
import matplotlib.pyplot as plt
import sys
import seaborn as sns
from sklearn.metrics import confusion_matrix
from keras.callbacks import EarlyStopping
from PreProcessing import *
from itertools import product
"""**Construyendo el modelo**"""

# CONSTRUYENDO EL MODELO
# define the keras model
batch_size = 512

def create_fully_connected_model(input_dim, hidden_num, output_num):
    """

    :param input_dim:
    :param hidden_num:
    :param output_num:
    :return: model
    """
    w_initializer = keras.initializers.HeNormal()
    b_initializer = keras.initializers.RandomUniform(minval=-0.5, maxval=+0.5)

    inputs = keras.Input(shape=(input_dim,))
    x = keras.layers.BatchNormalization()(inputs)
    x = keras.layers.Dense(hidden_num, input_dim=input_dim, activation='relu', bias_initializer=b_initializer,
                           kernel_initializer=w_initializer)(x)
    x = keras.layers.BatchNormalization()(x)
    outputs = keras.layers.Dense(output_num, bias_initializer=b_initializer, kernel_initializer=w_initializer,
                                 activation='softmax')(x)
    model = keras.Model(inputs=inputs, outputs=outputs, name="MLP_futbol")

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model

def create_convolutional_model(filters,kernel_size,n_steps, n_features, output_num):
    """
    https://machinelearningmastery.com/cnn-models-for-human-activity-recognition-time-series-classification/
    :param input_dim:
    :param hidden_num:
    :param output_num:
    :return:
    """
    w_initializer = keras.initializers.HeNormal()
    b_initializer = keras.initializers.RandomUniform(minval=-0.5, maxval=+0.5)

    model = keras.Sequential()
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation='relu', input_shape=(n_steps,
                                                                                                      n_features)))
    # model.add(keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation='relu'))
    # model.add(keras.layers.Dropout(0.5))

    model.add(keras.layers.MaxPooling1D(pool_size=2))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Dense(output_num, bias_initializer=b_initializer, kernel_initializer=w_initializer,
                                 activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


# plot diagnostic learning curves
def summarize_diagnostics(history, projectPath, index, win_size, overlap):
    # plot loss
    plt.subplot(211)
    plt.title('Cross Entropy Loss')
    plt.plot(history.history['loss'], color='blue', label='train')
    plt.plot(history.history['val_loss'], color='orange', label='validation')
    plt.legend()
    # plot accuracy
    plt.subplot(212)
    plt.title('Classification Accuracy')
    plt.plot(history.history['accuracy'], color='blue', label='train')
    plt.plot(history.history['val_accuracy'], color='orange', label='validation')
    plt.legend()
    plt.suptitle(f'win_size={win_size} overlap={overlap}', fontsize=16)
    plt.tight_layout()
    # plt.show()
    # save plot to file
    # filename = projectPath + sys.argv[0].split('/')[-1]
    # print('Saving figure: ' + filename)
    plt.savefig('AjusteHiperparametros' + f'/{tipo_red}/{num_units_hidden}_neuronas/win_size={win_size}_overlap'
                                          f'={overlap}_plot.png')
    plt.close()
#%%

# %%
if __name__ == "__main__":
    pruebas = 0
    tipo_red = 'Conv1D'
    if not pruebas:
        df_global = cargar_df_global()
        win_sizes = [25,30,35]
        overlaps = [10,15,20]
        list_num_units_hidden = [64,128,256]
    else:
        win_sizes = [35]
        overlaps = [15]
        list_num_units_hidden = [128]

    manager = Paths_manager()
    df_metricas_modelos = pd.DataFrame(columns=['win_size','overlap',
                                                'nº_evt_tr','nº_evt_val','nº_evt_te','nº_evt_tot',
                                                'nº_evtcerca_tr','nº_evtcerca_val','nº_evtcerca_te','nº_evtcerca_tot',
                                                'nº_NOevt_tr','nº_NOevt_val','nº_NOevt_te','nº_NOevt_tot',
                                                'train_accuracy','validation_accuracy','test_accuracy',
                                                'loss_train','loss_val','loss_test',
                                                'true0_pred0_tr','true0_pred1_tr','true0_pred2_tr',
                                                'true1_pred0_tr','true1_pred1_tr','true1_pred2_tr',
                                                'true2_pred0_tr','true2_pred1_tr','true2_pred2_tr',
                                                'true0_pred0_val','true0_pred1_val','true0_pred2_val',
                                                'true1_pred0_val','true1_pred1_val','true1_pred2_val',
                                                'true2_pred0_val','true2_pred1_val','true2_pred2_val',
                                                'true0_pred0_tes','true0_pred1_tes','true0_pred2_tes',
                                                'true1_pred0_tes','true1_pred1_tes','true1_pred2_tes',
                                                'true2_pred0_tes','true2_pred1_tes','true2_pred2_tes'])

    for num_units_hidden in list_num_units_hidden:
        for win_size in win_sizes:
            for overlap in overlaps:
                new_row_values = [win_size, overlap]
                print(f"Modelo con win_size={win_size} y overlap={overlap}")
                if pruebas:
                    ventanas_validacion = pd.read_csv('ventanas_validacion_35_15.csv')
                    ventanas_validacion.drop('Unnamed: 0',axis=1,inplace=True)
                    ventanas_test = pd.read_csv('ventanas_test_35_15.csv')
                    ventanas_test.drop('Unnamed: 0', axis=1, inplace=True)
                    ventanas_train = pd.read_csv('ventanas_train_35_15.csv')
                    ventanas_train.drop('Unnamed: 0', axis=1, inplace=True)
                else:
                    ventanas_train, ventanas_validacion, ventanas_test = generar_ventanas(overlap, win_size, \
                        df_global, manager)


                # split into input (X) and output (y) variables
                X_train = np.asarray(ventanas_train)[:, :-5]
                y_train = np.asarray(ventanas_train)[:, -3:]
                y_train_num = np.array([np.where(y_row == 1)[0] for y_row in y_train])

                X_val = np.asarray(ventanas_validacion)[:, :-5]
                y_val = np.asarray(ventanas_validacion)[:, -3:]
                y_val_num = np.array([np.where(y_row == 1)[0] for y_row in y_val])

                X_test = np.asarray(ventanas_test)[:, :-5]
                y_test = np.asarray(ventanas_test)[:, -3:]
                y_test_num = np.array([np.where(y_row == 1)[0] for y_row in y_test])

                num_unos_train = ventanas_train.loc[y_train[:,0] == 1,'EsEvento'].shape[0]
                num_unos_val = ventanas_validacion.loc[y_val[:,0] == 1,'EsEvento'].shape[0]
                num_unos_test = ventanas_test.loc[y_test[:,0] == 1,'EsEvento'].shape[0]
                num_unos_tot = num_unos_train+num_unos_val+num_unos_test
                print(f'Número total de casos EsEvento   ={num_unos_tot}    train={num_unos_train}  val'
                      f'={num_unos_val}  test={num_unos_test}')
                new_row_values += [num_unos_train,num_unos_val,num_unos_test,num_unos_tot]

                num_unos_train = ventanas_train.loc[y_train[:,1] == 1,'EventoCerca'].shape[0]
                num_unos_val = ventanas_validacion.loc[y_val[:,1] == 1,'EventoCerca'].shape[0]
                num_unos_test = ventanas_test.loc[y_test[:,1] == 1,'EventoCerca'].shape[0]
                num_unos_tot = num_unos_train+num_unos_val+num_unos_test
                print(f'Número total de casos EventoCerca={num_unos_tot}     train={num_unos_train} '
                      f'  val={num_unos_val}   test={num_unos_test}')
                new_row_values += [num_unos_train,num_unos_val,num_unos_test,num_unos_tot]

                num_unos_train = ventanas_train.loc[y_train[:,2] == 1,'NoEvento'].shape[0]
                num_unos_val = ventanas_validacion.loc[y_val[:,2] == 1,'NoEvento'].shape[0]
                num_unos_test = ventanas_test.loc[y_test[:,2] == 1,'NoEvento'].shape[0]
                num_unos_tot = num_unos_train+num_unos_val+num_unos_test
                print(f'Número total de casos NoEvento   ={num_unos_tot}   train={num_unos_train} val={num_unos_val} test={num_unos_test}')
                new_row_values += [num_unos_train,num_unos_val,num_unos_test,num_unos_tot]


                """**Entrenando el modelo**"""
                seed = 7
                # num_units_hidden = 128  # Number of neurons in the hidden layer
                num_epochs = 20
                num_CV = 5

                np.random.seed(seed)
                # kf = StratifiedKFold(n_splits=num_CV, shuffle=True, random_state=seed)

                list_models = []
                list_history = []
                list_train_accuracy = []
                list_train_loss = []
                list_test_accuracy = []
                list_test_loss = []
                counter_CV = 1
                PATH_CARPETA_RAIZ = 'Datos_jugadores'

                filter_size = 32 #[32,64]
                kernel_size = 3 #[2,3,4]
                if tipo_red == 'Fully_connected':
                    model = create_fully_connected_model(X_train.shape[1], num_units_hidden, y_train.shape[1])
                    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1,patience=5)
                    history = model.fit(X_train, y_train, epochs=num_epochs, validation_data=(X_val, y_val),callbacks=[es])
                elif tipo_red== 'Conv1D':
                    model = create_convolutional_model(filter_size, kernel_size, X_train.shape[1], 1, y_train.shape[1])
                    X_train_3dim = X_train.reshape((X_train.shape[0], X_train.shape[1], 1))
                    X_val_3dim = X_val.reshape((X_val.shape[0], X_val.shape[1], 1))
                    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1,patience=5)
                    history = model.fit(X_train_3dim, y_train, epochs=num_epochs, validation_data=(X_val_3dim,
                                                                                                   y_val),callbacks=[es])

                summarize_diagnostics(history, PATH_CARPETA_RAIZ, counter_CV,win_size,overlap)

                if tipo_red== 'Conv1D':
                    X_test_3dim = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))
                    loss_train, accuracy_train = model.evaluate(X_train_3dim, y_train, verbose=0)
                    loss_val, accuracy_val = model.evaluate(X_val_3dim, y_val, verbose=0)
                    loss_test, accuracy_test = model.evaluate(X_test_3dim, y_test, verbose=0)
                    print('Train Accuracy: %.2f' % (accuracy_train * 100))
                    print('Validation Accuracy: %.2f' % (accuracy_val * 100))
                    print('Test Accuracy: %.2f' % (accuracy_test * 100))
                elif tipo_red=='Fully_connected':
                    loss_train, accuracy_train = model.evaluate(X_train, y_train, verbose=0)
                    loss_val, accuracy_val = model.evaluate(X_val, y_val, verbose=0)
                    loss_test, accuracy_test = model.evaluate(X_test, y_test, verbose=0)
                    print('Train Accuracy: %.2f' % (accuracy_train * 100))
                    print('Validation Accuracy: %.2f' % (accuracy_val * 100))
                    print('Test Accuracy: %.2f' % (accuracy_test * 100))

                new_row_values += [accuracy_train,accuracy_val,accuracy_test,loss_train,loss_val,loss_test]

                # # serialize model to JSON
                # model_json = model.to_json()
                # with open("model_" + str(counter_CV) + ".json", "w") as json_file:
                #     json_file.write(model_json)
                # # serialize weights to HDF5
                # model.save_weights("model_" + str(counter_CV) + ".h5")
                # print("Saved model to disk")

                # counter_CV = counter_CV + 1
                '''Genero las matrices de confusión'''
                class_labels = ['0', '1', '2']
                conf_mats_values = []
                for valores_X,valores_y,etiqueta,df_ventanas in [(X_train,y_train,'train',ventanas_train),
                                                                 (X_val,y_val,'validation',ventanas_validacion),
                                                                 (X_test,y_test,'test',ventanas_test)]:
                    # model = list_models[counter_CV]
                    predictions = model.predict(valores_X)
                    max_test = np.argmax(valores_y, axis=1)
                    max_predictions = np.argmax(predictions, axis=1)

                    df_ventanas['argmax_test'] = max_test
                    df_ventanas['argmax_predictions'] = max_predictions

                    fig = px.line(df_ventanas, x=df_ventanas['seq_number'],
                                  y=['argmax_test','argmax_predictions'],
                                  line_shape='hv', title=f"WinSiz={win_size}Ovrlp={overlap}filter={filter_size}"
                                                         f"kernel={kernel_size}-{etiqueta}")
                    plot(fig)

                    conf_matrix = confusion_matrix(max_test, max_predictions,normalize='true')
                    sns.heatmap(conf_matrix, xticklabels=class_labels, yticklabels=class_labels, annot=True, linewidths=0.1,
                                cmap='YlGnBu')#fmt='d',
                    plt.title(
                        f"WinSize={win_size} Overlap={overlap} filter={filter_size} kernel={kernel_size} - {etiqueta}",
                        fontsize=15)
                    plt.ylabel('True label')
                    plt.xlabel('Predicted label')
                    plt.tight_layout()
                    # plt.show()
                    plt.savefig('AjusteHiperparametros'+ f'/{tipo_red}'
                                                         f'/{num_units_hidden}_neuronas/ConfusionMatrix_win_size={win_size}_ove'
                                                                            f'rlap={overlap}_{etiqueta}.png')
                    plt.close()
                    counter_CV = counter_CV + 1

                    conf_mats_values+=list(conf_matrix[0]) + list(conf_matrix[1]) + list(conf_matrix[2])
                    # dos_pies_df['eventos'] = dos_pies_df['eventos'] * 600
                    # fig = px.line(dos_pies_df, x=dos_pies_df.index, y=['gyro_y_r', 'gyro_y_l', 'eventos'])
                    # plot(fig)
                new_row_values+=conf_mats_values
                new_row = dict(zip(df_metricas_modelos.columns,new_row_values))
                df_metricas_modelos = pd.concat([df_metricas_modelos, pd.DataFrame([new_row])], ignore_index=True)

                # serialize model to JSON
                model_json = model.to_json()
                nombre_file = 'AjusteHiperparametros'+ f'/{tipo_red}/{num_units_hidden}_neuronas/model_{tipo_red}_WinSize={win_size}_Overlap={overlap}_filter={filter_size}_kernel={kernel_size}'
                with open(nombre_file + '.json', "w") as json_file:
                    json_file.write(model_json)
                # serialize weights to HDF5
                model.save_weights(nombre_file + ".h5")
                print("Saved model to disk")

        # df_metricas_modelos.to_csv(f'AjusteHiperparametros/{tipo_red}/{num_units_hidden}_neuronas/metricas_modelos.csv')
        pass
        save_cmd = input('Pulsa s para guardar el modelo generado:')
        if save_cmd == 's' or save_cmd=='S':
            # serialize model to JSON
            model_json = model.to_json()
            nombre_file = f"model_{tipo_red}_WinSize={win_size}_Overlap={overlap}_filter={filter_size}_kernel={kernel_size}"
            with open(nombre_file+'.json',"w") as json_file:
                json_file.write(model_json)
            # serialize weights to HDF5
            model.save_weights(nombre_file+".h5")
            print("Saved model to disk")
#     ToDo: Hacer una carpeta llamada último modelo en la que se guarden los pesos y las gráficas obtenidas para el
#      último modelo que se haya testeado para tenerlas más a mano y para poder repetirlo en caso de necesidad.
