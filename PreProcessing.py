"""**Imports**"""

import numpy as np
import pandas as pd
import scipy
from plotly.offline import plot
import plotly.express as px
import os
import questionary
import random
from scipy.signal import argrelextrema

class Paths_manager():
    PATH_CARPETA_RAIZ = 'Datos_jugadores'
    lista_ID_jugadores = [jugador for jugador in os.listdir(PATH_CARPETA_RAIZ) if len(jugador)==2]
    PATH_PARQUET = 'datos IMU procesados alvaro'#'parquet'
    PATH_LIMITES = '_excel_inicio_fin'
    paths_df = pd.DataFrame(columns=['jugador','prueba','path_prueba','path_limites_l','path_limites_r'])

    def select_player_test(self):
        jugadores_seleccionados = questionary.checkbox(
            "Selecciona los JUGADORES que quieras analizar:",
            choices=self.lista_ID_jugadores
        ).ask()
        for jugador in jugadores_seleccionados:
            path_jugador = '/'.join([self.PATH_CARPETA_RAIZ, jugador, self.PATH_PARQUET])
            lista_archivos_pruebas = os.listdir(path_jugador)

            archivos_IMU_seleccionados = questionary.checkbox(
                "Selecciona los originales de la prueba que quieras analizar:",
                choices=lista_archivos_pruebas
            ).ask()  # returns value of selection

            for archivo_IMU in archivos_IMU_seleccionados:
                nombre_archivo = archivo_IMU.split('-')[0] if '-' in archivo_IMU else archivo_IMU.split('_')[0]

                new_row = pd.Series({'jugador': jugador,
                                     'prueba': archivo_IMU,
                                     'path_prueba': '/'.join([path_jugador, archivo_IMU]),
                                     'path_limites_l':'/'.join([self.PATH_CARPETA_RAIZ, jugador, '_excel_inicio_fin',
                                                                nombre_archivo+'_left.csv']),
                                     'path_limites_r':'/'.join([self.PATH_CARPETA_RAIZ, jugador, '_excel_inicio_fin',
                                                                nombre_archivo+'_right.csv']),
                                     'path_save_sync':'/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync',
                                                                nombre_archivo + '.parquet'])})
                isExist = os.path.exists('/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync']))
                if not isExist:
                    os.makedirs('/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync']))
                self.paths_df = pd.concat([self.paths_df, new_row.to_frame().T], ignore_index=True)

    def select_all(self):
        for jugador in self.lista_ID_jugadores:
            path_jugador = '/'.join([self.PATH_CARPETA_RAIZ, jugador, self.PATH_PARQUET])
            lista_archivos_pruebas = os.listdir(path_jugador)

            archivos_IMU_seleccionados = questionary.checkbox(
                "Selecciona los originales de la prueba que quieras analizar:",
                choices=lista_archivos_pruebas
            ).ask()  # returns value of selection

            for archivo_IMU in archivos_IMU_seleccionados:
                nombre_archivo = archivo_IMU.split('-')[0] if '-' in archivo_IMU else archivo_IMU.split('_')[0]

                new_row = pd.Series({'jugador': jugador,
                                     'prueba': archivo_IMU,
                                     'path_prueba': '/'.join([path_jugador, archivo_IMU]),
                                     'path_limites_l': '/'.join([self.PATH_CARPETA_RAIZ, jugador, '_excel_inicio_fin',
                                                                 nombre_archivo + '_left.csv']),
                                     'path_limites_r': '/'.join([self.PATH_CARPETA_RAIZ, jugador, '_excel_inicio_fin',
                                                                 nombre_archivo + '_right.csv']),
                                     'path_save_sync': '/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync',
                                                                 nombre_archivo + '.parquet'])})
                isExist = os.path.exists('/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync']))
                if not isExist:
                    os.makedirs('/'.join([self.PATH_CARPETA_RAIZ, jugador, '_parquet_sync']))
                self.paths_df = pd.concat([self.paths_df, new_row.to_frame().T], ignore_index=True)


def juntar_DFs_sincronizados(row_jugador):
    '''Al tomar los datos hay valores que faltan, esta función utiliza el timestamp para generar un índice que incluya
    esos valores que se pierden. También actualizan los valores de los rectángulos de eventos para tenerlos en base al
    nuevo índice.
    :param  row_jugador contiene todos los datos para poder acceder a los diferentes DFs de cada jugador
    :return dos_pies_df contiene los datos de los dos pies sincronizados con el nuevo índice
    '''
    og_df = pd.read_parquet(row_jugador['path_prueba'], engine='pyarrow')
    # Separar por pies
    mask_left = og_df['foot'].eq('left')
    mask_right = og_df['foot'].eq('right')
    l_df = og_df.loc[mask_left].reset_index(drop=True)
    r_df = og_df.loc[mask_right].reset_index(drop=True)

    if 'CambiosDeDireccion' in row_jugador.prueba:
        # Abro el DF que marca las tandas
        path_tandas = f'D:/Documents/_GitLab/procesado_futbol/Datos_jugadores/' \
                      f'{row_jugador.jugador}/_graficas/CambiosDeDireccion'
        lista_archivos_tandas = os.listdir(path_tandas)
        for archivo in lista_archivos_tandas:
            if 'csv' in archivo:
                tandas_name = archivo
                pie_tandas = archivo.split('_')[3]
                break
        archivo_tandas = pd.read_csv('/'.join([path_tandas, tandas_name]))
        # Si es uno de los casos grabados en dos partes hay que separar, ya que se enventanó en un solo archivo
        if len(archivo_tandas)!=len(r_df if pie_tandas=='dch' else l_df):
            if '02' in row_jugador['path_prueba']:
                inicio_df_tandas = len(archivo_tandas)-len(r_df if pie_tandas=='dch' else l_df)
                archivo_tandas = archivo_tandas.iloc[inicio_df_tandas:].reset_index(drop=True)
            else:
                final_df_tandas = len(r_df if pie_tandas=='dch' else l_df)
                archivo_tandas = archivo_tandas.iloc[:final_df_tandas].reset_index()

        limites_tandas = archivo_tandas.index[archivo_tandas['evento'] == 1].tolist() + [len(r_df if pie_tandas=='dch' else l_df)]
        DIFERENCIAS = np.diff([0]+limites_tandas[2::2]+[len(r_df if pie_tandas=='dch' else l_df)])
        ID_tandas = []
        ID_tipo_tanda = []
        for ID, num_elementos in enumerate(DIFERENCIAS):
            ID_tandas += [ID] * num_elementos

        tipo = 'a'
        for num_elementos in np.diff([0]+limites_tandas):
            tipo = 'b' if tipo == 'a' else 'a'
            ID_tipo_tanda += [tipo] * num_elementos

        if pie_tandas == 'dch':
            r_df['num_tanda'] = ID_tandas
            r_df['tipo_tanda'] = ID_tipo_tanda
        else:
            l_df['num_tanda'] = ID_tandas
            l_df['tipo_tanda'] = ID_tipo_tanda

    # Cargar los archivos que limitan los eventos
    pies_jugador = []
    for pie, identificador in ((l_df, 'left'), (r_df, 'right')):
        if 'CambiosDeDireccion' in row_jugador.prueba:
            limites = pd.read_csv(row_jugador['path_limites_l'] if identificador == 'left' else row_jugador['path_limites_r'], sep=',').drop(['Unnamed: 0'], axis=1)
            # Añadir columnas eventos
            pie['eventos'] = [np.nan] * len(pie)
            pie.loc[0, 'eventos'] = 0
            # Poner 1 si la los datos corresponden a un evento 0 si no es así
            for index, row_limites in limites.iterrows():
                pie.loc[row_limites['inicio'], 'eventos'] = 1
                pie.loc[row_limites['final'], 'eventos'] = 0
            pie['eventos'].ffill(inplace=True)
            pie['eventos'] = pie['eventos'].astype(int)

    #   Completar huecos de los dfs
        # Generar nuevo índice compensando los números de secuencia perdidos
        #Identifico los cambios de 255 a 0
        pie['seq_iteration'] = pie['seq_number'].diff()
        pie.loc[pie['seq_iteration'] > 0, 'seq_iteration'] = None
        pie.loc[pie['seq_iteration'] <= 0, 'seq_iteration'] = 1
        #Indico el número de veces que se ha cambiado de 255 a 0
        pie['seq_iteration'] = pie['seq_iteration'].cumsum().ffill().fillna(0)
        #Multiplico por 256 y sumo el número de secuencia, ahora todavía hay valores que no existen, los saltos
        pie['new_seq_number'] = (pie['seq_iteration'] * 256 + pie['seq_number']).astype(int)
        #Cambio el índice por una lista que vaya desde el primer hasta el último valor de new_seq_number,
        # aquí se rellenan los huecos y se crean nuevas filas vacías
        new_pie = (pie.set_index('new_seq_number').reindex(range(pie['new_seq_number'].iat[0],
                                                                 pie['new_seq_number'].iat[-1] + 1),
                                                                 fill_value=None).reset_index())
        # Interpolar para completar las filas vacías generadas
        for column in new_pie.columns:
            if column in ['insole', 'foot', 'eventos', 'num_tanda', 'tipo_tanda']:
                new_pie[column] = new_pie[column].ffill()
            else:
                new_pie[column].interpolate(inplace=True) #method='polynomial', order=3,
        if identificador=='right' and len(new_pie)>len(pies_jugador[-1]):
            pies_jugador.append(new_pie)
        else:
            pies_jugador.insert(0,new_pie)

    dos_pies_df = pd.merge_asof(pies_jugador[0].sort_values('timestamp'), pies_jugador[1].sort_values('timestamp'), on='timestamp', suffixes=['_l', '_r'])
    dos_pies_df['player_ID'] = [row_jugador.jugador]*len(dos_pies_df)
    if 'CambiosDeDireccion' in row_jugador.prueba:
        dos_pies_df['eventos'] = dos_pies_df['eventos_r'] + dos_pies_df['eventos_l']
        dos_pies_df.drop(['insole_r', 'insole_l', 'foot_r', 'foot_l','eventos_r','eventos_l'], axis=1, inplace=True)
        dos_pies_df.drop(index=dos_pies_df[np.isnan(dos_pies_df['eventos'])].index.tolist(), inplace=True)
    else:
        dos_pies_df.drop(['insole_r', 'insole_l', 'foot_r', 'foot_l'], axis=1, inplace=True)


    return dos_pies_df

def cargar_df_sincronizado(row_jugador):
    '''Cambia el formato del DF para tener en las mismas filas los datos de ambos pies.
    :param  row_jugador contiene todos los datos para poder acceder a los diferentes DFs de cada jugador
    :return dos_pies_df contiene los datos de los dos pies sincronizados con el nuevo índice
    '''
    og_df = pd.read_parquet(row_jugador['path_prueba'], engine='pyarrow')
    # Separar por pies
    mask_left = og_df['foot'].eq('left')
    mask_right = og_df['foot'].eq('right')
    l_df = og_df.loc[mask_left]
    r_df = og_df.loc[mask_right]
    rounded_index_l = pd.Series([round(elem, 2) for elem in l_df.index])
    rounded_index_r = pd.Series([round(elem, 2) for elem in r_df.index])
    l_df.reset_index(drop=True, inplace=True)
    r_df.reset_index(drop=True, inplace=True)
    l_df['absolute_time'] = l_df.absolute_time.round(2)
    r_df['absolute_time'] = r_df.absolute_time.round(2)
    dos_pies = pd.merge(l_df, r_df, on="absolute_time", suffixes=['_l', '_r'])
    return dos_pies
def get_inicio_y_duracion_eventos(df):
    mask_unos = df['eventos'].eq(1)
    index_unos = df.index[mask_unos].tolist()
    control_consecutivos = np.diff(index_unos)
    index_ini_fin_eventos = [0] + [i + 1 for i in range(len(control_consecutivos) - 1) if control_consecutivos[i] != control_consecutivos[i + 1]]
    duracion = np.array(index_ini_fin_eventos[1::2]) - np.array(index_ini_fin_eventos[:-2:2]) + 1
    index_inicios_eventos = np.array(index_unos)[index_ini_fin_eventos[::2]]
    return duracion, index_inicios_eventos

def train_validacion_test_split(df_datos_sync,manager,jugador_test='09',quitar_parte_b=True):
    if quitar_parte_b:
        df_test  = df_datos_sync[df_datos_sync['player_ID']==jugador_test][df_datos_sync['tipo_tanda']=='a']
        df_train = df_datos_sync[df_datos_sync['player_ID']!=jugador_test][df_datos_sync['tipo_tanda']=='a']
    # Todo: quitar las partes b de las tandas para reducir el número de datos sin evento
    else:
        df_test  = df_datos_sync[df_datos_sync['player_ID']==jugador_test]
        df_train = df_datos_sync[df_datos_sync['player_ID']!=jugador_test]

    tandas_por_jugador = [df_train[(df_train['player_ID'] == ID)]['num_tanda'].max() for ID in
                          manager.lista_ID_jugadores if ID != jugador_test]
    num_total_tandas = sum(tandas_por_jugador)
    # Se seleccionan el 20% de las tandas de train para validación
    # Primero se seleccionan de forma aleatoria los jugadores a seleccionar
    elecciones_jugadores_validacion = random.choices([ID for ID in manager.lista_ID_jugadores if ID != jugador_test],
                                          k=int(num_total_tandas*0.2))
    # Después se seleccionan de forma aleatoria las tandas de los jugadores seleccionados anteriormente
    elecciones_tandas_validacion = []
    for jugador in elecciones_jugadores_validacion:
        elecciones_tandas_validacion.append(random.randrange(tandas_por_jugador[int(jugador)-1]))
    # Con las elecciones aleatorias anteriores se extraen las tandas para validación
    df_validacion = pd.DataFrame()
    for player_ID,num_tanda in zip(elecciones_jugadores_validacion,elecciones_tandas_validacion):
        df_train_tmp = df_train[(df_train['player_ID']==player_ID)&(df_train['num_tanda']==float(num_tanda))]
        df_validacion = pd.concat([df_validacion,df_train_tmp])
        df_train.drop(df_train_tmp.index,inplace=True)

    df_train.to_parquet('df_train.parquet')
    df_validacion.to_parquet('df_validacion.parquet')
    df_test.to_parquet('df_test.parquet')

    return df_train, df_validacion, df_test




def generar_ventanas(overlap,win_size,df_datos_sync,manager,medidas_interes=['gyro_y_l','gyro_z_l','acc_z_l','gyro_y_r',
                                                                     'gyro_z_r','acc_z_r']):

    df_train, df_validacion, df_test = train_validacion_test_split(df_datos_sync,manager, jugador_test='09')
    def add_evt_percentage(df):
        a = df['eventos'] != 0
        df1 = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
        df1 = df1.fillna(0)
        df_inicios_tamaños = pd.DataFrame(df1)
        df_inicios_tamaños['diff'] = df_inicios_tamaños['eventos'].gt(0)
        df_inicios_tamaños['fin_bien'] = (df_inicios_tamaños['diff'].shift(-1) ^ df_inicios_tamaños['diff']) & \
                                         df_inicios_tamaños['diff']
        df_inicios_tamaños['ini_bien'] = (df_inicios_tamaños['diff'].shift(1) ^ df_inicios_tamaños['diff']) & \
                                         df_inicios_tamaños['diff']
        tamaños_eventos = pd.DataFrame(df_inicios_tamaños[df_inicios_tamaños['fin_bien']]['eventos']).reset_index()
        for i,row in enumerate(tamaños_eventos.iterrows()):
            df_inicios_tamaños.loc[row[1]['index']-row[1]['eventos']+1:row[1]['index']] = 1/row[1]['eventos']*100
        return df_inicios_tamaños['eventos']

    df_train['percentage_evt'] = add_evt_percentage(df_train)
    df_test['percentage_evt'] = add_evt_percentage(df_test)
    df_validacion['percentage_evt'] = add_evt_percentage(df_validacion)

    def aplanar_df_datos(df,win_size,overlap):
        step = int(win_size*overlap/100)
        # Compruebo que no haya muestras que junten tandas o jugadores diferentes
        inicios = []
        for i in [*range(int((df.shape[0]-win_size) / step))]:
            if (df.iloc[step * i]['player_ID']==df.iloc[step * i + win_size]['player_ID']) & \
               (df.iloc[step * i]['num_tanda']==df.iloc[step * i + win_size]['num_tanda']):
                inicios.append(step * i)
        # Genero un Dataframe que contenga los inicios y finales de las ventanas en formato plano y en formato DF
        ini_fin_filas_y_columnas = pd.DataFrame()
        num_columnas_por_row = len(medidas_interes)
        ini_fin_filas_y_columnas['ini_row'] = inicios
        ini_fin_filas_y_columnas['fin_row'] = ini_fin_filas_y_columnas['ini_row'] + win_size
        ini_fin_filas_y_columnas['ini_col'] = ini_fin_filas_y_columnas['ini_row'] * num_columnas_por_row
        ini_fin_filas_y_columnas['fin_col'] = ini_fin_filas_y_columnas['fin_row'] * num_columnas_por_row
        # Añado los nombres de las columnas al dataframe añadiendo un número correspondiente al nº de muestra dentro de
        # la ventana
        columnas_ventanas = []
        for num_col in [*range(win_size)]:
            columnas_ventanas += [medida + f'_{num_col}' for medida in medidas_interes]
        columnas_ventanas.append('percentage_evt')
        columnas_ventanas.append('seq_number')
        # Extraigo los datos y los añado al dataframe final
        df_trabajo = df[df.columns.intersection(['new_seq_number_l','player_ID','num_tanda','tipo_tanda',
                                                 'percentage_evt'])]
        plano = np.asarray(df[df.columns.intersection(medidas_interes)]).ravel().tolist()
        datos_planos = []
        for row in ini_fin_filas_y_columnas.iterrows():
            datos_planos.append(plano[row[1]['ini_col']:row[1]['fin_col']] +
                                [df_trabajo.iloc[row[1]['ini_row']:row[1]['fin_row']]['percentage_evt'].cumsum().max(),
                                 df_trabajo.iloc[row[1]['ini_row']:row[1]['fin_row']]['new_seq_number_l'].mean().astype(int) ])

        df_ventanas = pd.DataFrame(datos_planos,columns=columnas_ventanas)
        df_ventanas['EsEvento'] = df_ventanas['percentage_evt'].gt(float(30))
        df_ventanas['EsEvento'] = df_ventanas['EsEvento'].replace({True: 1, False: 0})
        df_ventanas['EventoCerca'] = df_ventanas['percentage_evt'].gt(float(0)) & df_ventanas[
            'percentage_evt'].le(float(30))
        df_ventanas['EventoCerca'] = df_ventanas['EventoCerca'].replace({True: 1, False: 0})
        df_ventanas['NoEvento'] = df_ventanas['percentage_evt'].eq(float(0))
        df_ventanas['NoEvento'] = df_ventanas['NoEvento'].replace({True: 1, False: 0})
        return df_ventanas

    df_ventanas_train = aplanar_df_datos(df_train,win_size,overlap)

    df_ventanas_test = aplanar_df_datos(df_test, win_size, overlap)
    df_ventanas_validacion = aplanar_df_datos(df_validacion, win_size, overlap)
    return df_ventanas_train,df_ventanas_validacion,df_ventanas_test

def aplanar_df_datos_desconocidos(df,win_size,overlap,medidas_interes=['gyro_y_l','gyro_z_l','acc_z_l','gyro_y_r',
                                                                     'gyro_z_r','acc_z_r']):
    step = int(win_size*overlap/100)
    # Compruebo que no haya muestras que junten tandas o jugadores diferentes
    inicios = []
    for i in [*range(int((df.shape[0]-win_size) / step))]:
        inicios.append(step * i)
    # Genero un Dataframe que contenga los inicios y finales de las ventanas en formato plano y en formato DF
    ini_fin_filas_y_columnas = pd.DataFrame()
    num_columnas_por_row = len(medidas_interes)
    ini_fin_filas_y_columnas['ini_row'] = inicios
    ini_fin_filas_y_columnas['fin_row'] = ini_fin_filas_y_columnas['ini_row'] + win_size
    ini_fin_filas_y_columnas['ini_col'] = ini_fin_filas_y_columnas['ini_row'] * num_columnas_por_row
    ini_fin_filas_y_columnas['fin_col'] = ini_fin_filas_y_columnas['fin_row'] * num_columnas_por_row
    # Añado los nombres de las columnas al dataframe añadiendo un número correspondiente al nº de muestra dentro de
    # la ventana
    columnas_ventanas = []
    for num_col in [*range(win_size)]:
        columnas_ventanas += [medida + f'_{num_col}' for medida in medidas_interes]
    # columnas_ventanas.append('seq_number')
    # Extraigo los datos y los añado al dataframe final
    plano = np.asarray(df[df.columns.intersection(medidas_interes)]).ravel().tolist()
    datos_planos = []
    for row in ini_fin_filas_y_columnas.iterrows():
        datos_planos.append(plano[row[1]['ini_col']:row[1]['fin_col']])

    df_ventanas = pd.DataFrame(datos_planos,columns=columnas_ventanas)
    return df_ventanas



def generar_df_global(manager):
    manager.select_all()
    df_global = pd.DataFrame()
    for i,row_jugador in manager.paths_df.iterrows():
        dos_pies_df = juntar_DFs_sincronizados(row_jugador)
        if '02-' in row_jugador['prueba']:
            offset = df_global.iloc[-1]['num_tanda'] + 1
            dos_pies_df['num_tanda'] += offset
        df_global = pd.concat([df_global, dos_pies_df], ignore_index=True)
        dos_pies_df.to_parquet(row_jugador['path_save_sync'])
    df_global.to_parquet('df_global.parquet')
    # df_global.to_csv('df_global.csv')
    return df_global

def cargar_df_global():
    return pd.read_parquet('df_global.parquet')


# %%
if __name__ == "__main__":
    manager = Paths_manager()
    # df_global = generar_df_global(manager)
    df_global = cargar_df_global()
    win_size = 30
    overlap = 20
    ventanas_train,ventanas_validacion,ventanas_test = generar_ventanas(35, 15, df_global,manager)
    ventanas_train.to_parquet(f'ventanas_train_ws{win_size}_ol{overlap}.parquet')
    ventanas_validacion.to_parquet(f'ventanas_validacion_ws{win_size}_ol{overlap}.parquet')
    ventanas_test.to_parquet(f'ventanas_test_ws{win_size}_ol{overlap}.parquet')


    # df_global = pd.DataFrame()
    # manager.select_player_test()
    # for i,row_jugador in manager.paths_df.iterrows():
    #     dos_pies_df = juntar_DFs_sincronizados(row_jugador)
    #     df_global = pd.concat([df_global, dos_pies_df], ignore_index=True)
    #
    #     # dos_pies_df['eventos'] = dos_pies_df['eventos'] * 600
    #     # fig = px.line(dos_pies_df, x=dos_pies_df.index, y=['gyro_y_r', 'gyro_y_l', 'eventos'])
    #     # plot(fig)
    #
    #     dos_pies_df.to_parquet(row_jugador['path_save_sync'])
    # df_global.to_parquet('df_global.parquet')
    # df_global.to_csv('df_global.csv')

    pass
