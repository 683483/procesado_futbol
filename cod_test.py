import os.path

import plotly.express as px
from plotly.offline import  plot
import pandas as pd


def find_consecutive_values(data:pd.DataFrame, col, values):

    for k, v in data[data[col].isin(values)].groupby((~data[col].isin(values)).cumsum()):
        data.loc[v.index, f'consecutive'] = v.index.size

    return data


def check_density(data:pd.DataFrame, col, value):

    target = data[col].eq(value)
    target = 2 - data[col]
    density = target.rolling(15, center=True).mean()
    return density




def test_evaluation():
    base_path = r'C:\Users\algaroche\OneDrive - unizar.es\Desarrollo - Algoritmia\Fútbol\Modelo convolucional'
    filename = os.path.join(base_path, 'df_con_ytest-ypred.csv')

    data = pd.read_csv(filename)

    data = find_consecutive_values(data, 'argmax_predictions', [0])
    data['density'] = check_density(data, 'argmax_predictions', 0)
    data['target_density'] = check_density(data, 'argmax_test', 0)
    data['consecutive_count'] = 0
    data.loc[data['consecutive'].ge(6), 'consecutive_count'] = 3
    fig = px.line(data, y=['argmax_test','argmax_predictions', 'consecutive', 'density', 'target_density', 'consecutive_count'], markers='.')
    plot(fig)
    fig = px.line(data, y=['argmax_test', 'consecutive_count'], markers='.')
    plot(fig)
    pass


if __name__ == '__main__':
    test_evaluation()