import pandas as pd
import csv
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from math import isnan
from plotly.offline import plot
import os
import questionary
from datetime import datetime
def carga_datos(ruta_datos_IMU):  # , ruta_datos_crono
    '''
    Abre un archivo parquet o csv (y genera el parquet), guarda en dataframe y lo divide en pie derecho e izquierdo.
    Parameters:
        ruta_datos_IMU : ruta del archivo a analizar
    Returns:
        df_pieDch : dataframe con datos del pie derecho
        df_pieIzq : dataframe con datos del pie izquierdo
    '''
    carpetas = ruta_datos_IMU.split('/')


    if 'datos IMU' in carpetas:
        df_IMU_data = pd.read_csv(ruta_datos_IMU, sep=',')
        indice_parquet = carpetas.index('datos IMU')
        carpetas.remove('datos IMU')
        carpetas.insert(indice_parquet, 'parquet')
        parquet_path = '/'.join(carpetas[:-1])
        if not os.path.exists(parquet_path):
            os.makedirs(parquet_path)
        # parquet_para_guardar = df_IMU_data.to_parquet(parquet_path +'/'+ (carpetas[-1])[:-3] + 'parquet')
    elif 'parquet' in carpetas:
        df_IMU_data = pd.read_parquet(ruta_datos_IMU, engine='pyarrow')
    # separo el dataframe por pie izquierdo y derecho
    mask = (df_IMU_data['foot'] == 'right')
    df_pieDch = df_IMU_data.loc[mask].reset_index(drop=True)
    mask = (df_IMU_data['foot'] == 'left')
    df_pieIzq = df_IMU_data.loc[mask].reset_index(drop=True)
    return df_pieDch, df_pieIzq


def carga_datos_csv(ruta_datos_IMU):
    '''
    Abre un csv, guarda en dataframe y lo divide en pie derecho e izquierdo.
    Parameters:
        ruta_datos_IMU : ruta del archivo a analizar
    Returns:
        df_pieDch : dataframe con datos del pie derecho
        df_pieIzq : dataframe con datos del pie izquierdo
    '''
    df_IMU_data = pd.read_csv(ruta_datos_IMU)
    df_IMU_data.to_parquet(ruta_datos_IMU[:-3] + 'parquet')
    # separo el dataframe por pie izquierdo y derecho
    mask = (df_IMU_data['foot'] == 'right')
    df_pieDch = df_IMU_data.loc[mask].reset_index(drop=True)
    mask = (df_IMU_data['foot'] == 'left')
    df_pieIzq = df_IMU_data.loc[mask].reset_index(drop=True)
    return df_pieDch, df_pieIzq


def print_all_data(path_imu, medida, test_name, player_ID, path_save=None):
    '''
    Abre un csv y grafica la medida indicada. Si se introduce una ruta, guarda la gráfica ahí.
    Parameters:
        path_imu : ruta del archivo a analizar
        medida : medida a graficar
        test_name : nombre del test de los datos correspondientes
        player_ID : Identificador del jugador
        path_save : ruta para guardar la gráfica
    '''
    df_pieDch, df_pieIzq = carga_datos(path_imu)
    repetir_deteccion = 1
    while repetir_deteccion:
        repetir_deteccion = 0
        for data in [df_pieDch, df_pieIzq]:
            data['tiempo_relativo'] = [x * 0.02 for x in [*range(len(data))]]
            x = [k for k in [*range(len(data))]]#data['tiempo_relativo']
            if medida == 'acc':
                row_titles = ['acx', 'gyry', 'acz']#['acx', 'acy', 'acz']
            elif medida == 'gyro':
                row_titles = ['gyrx', 'gyry', 'gyrz']
            n_plots = len(row_titles)
            fig = make_subplots(n_plots, 1, shared_xaxes=True, x_title='Time (s)', row_titles=row_titles,
                                vertical_spacing=0.02, row_heights=[1 / n_plots] * n_plots)
            fig.add_trace(go.Scatter(x=x, y=data[row_titles[0]]), row=1, col=1)
            fig.add_trace(go.Scatter(x=x, y=data[row_titles[1]]), row=2, col=1)
            fig.add_trace(go.Scatter(x=x, y=data[row_titles[2]]), row=3, col=1)

            title = f'{test_name} Player: {player_ID} ' + ' ' + data['foot'].loc[0] + ' ' + medida
            fig.update_layout(title_text=title, title_x=0.5)
            if path_save is not None:
                if not os.path.isdir(path_save):
                    os.mkdir(path_save)
                path_save_1 = '/'.join([path_save,test_name])
                if not os.path.isdir(path_save_1):
                    os.mkdir(path_save_1)
                fig.write_html('/'.join([path_save_1,f"/{medida}_{data['foot'].loc[0]}.html"]))
            plot(fig)


def find_flat_p(signal: pd.Series, window=10, ratio=1_000):
    '''
    Calcula el valor absoluto de la diferencia con el anterior por el valor actual. Aplica
    una ventana deslizante de window valores y hace la media de los valores de la ventana.
    Se hace la media de todos los valores medios y lo divide por ratio para obtener
    el threshold up_th que se utilizará para compararlo con los valores de signal_diff_prod_w
    y considerar si hay o no movimiento.
    Parameters:
        signal : señal a analizar para detectar movimiento
        window : tamaño de la ventana con la que se hace la media
        ratio : parámetro que ajusta el orden de magnitud del threshold
    Returns:
        flat_data_id : lista de 1 y 0 para indicar si hay o no movimiento
        signal_diff_prod_w : lista con diferencia con valor anterior y multiplicación por el mismo tras hacer la ventana
        up_th : threshold obtenido
    '''
    signal_diff = signal.diff().fillna(0)
    signal_diff_prod = (signal * signal_diff).abs()
    signal_diff_prod_w = signal_diff_prod.rolling(window, center=True, min_periods=1).mean()
    up_th = signal_diff_prod_w.mean() / ratio
    flat_data_id = signal_diff_prod_w.le(up_th)

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=[*range(len(signal))], y=signal, name='Datos raw'))
    fig.add_trace(go.Scatter(x=[*range(len(signal_diff_prod))], y=signal_diff_prod, name='Señal diferencias'))
    fig.add_trace(go.Scatter(x=[*range(len(signal_diff_prod_w))], y=signal_diff_prod_w, name='Con ventana'))
    fig.add_trace(go.Scatter(x=[*range(len(signal_diff_prod_w))], y=[up_th]*len(signal_diff_prod_w), name='Threshold'))
    fig.add_trace(go.Scatter(x=[*range(len(signal_diff_prod_w))], y=flat_data_id.astype(int), name='Superan Thr'))
    # fig.update_layout(title_text=title, title_x=0.5)
    plot(fig)

    return flat_data_id, signal_diff_prod_w, up_th


# Hace una comparación con histéresis para aplicar un filtrado a la detección de movimiento
def hystheresis_threshold(signal_diff_prod_w: pd.Series, up_th, hyst_up=10):
    '''
    Hace una comparación con histéresis para aplicar un filtrado a la detección de movimiento.
    Comprueba que los valores de la signal_diff_prod_w sean superiores al threshold up_th.
    Para cada valor se comprueba que se encuentre centrado en una ventana con hyst_up valores
    superiores al threshold. Se guarda en una lista los indices en los que se produce el cambio
    de zona con movimiento a zona sin.
    Parameters:
        signal_diff_prod_w : resultado de la diferencia con el anterior y el producto por él mismo
        up_th : valor a superar por signal_diff_prod_w para considerar movimiento
        hyst_up : número de valores en la ventana
    Returns:
        flat_data_id : lista de 1 y 0 para indicar si hay o no movimiento
        changes_array : lista de índices para indicar inicios y finales de movimiento
    '''
    signal_greater = signal_diff_prod_w.gt(up_th).astype(int)
    flat_data = signal_greater.rolling(hyst_up, center=True, min_periods=1).mean()
    flat_data_id = flat_data.eq(1).astype(int)
    changes_array = []
    element_ant = 0
    for num, element in enumerate(flat_data_id):
        if element_ant != element:
            changes_array.append(num)
        element_ant = element
    return flat_data_id, changes_array


def pairwise(iterable):
    '''
    Agrupa elementos de una lista de dos en dos
    Parameters:
        iterable : lista de elementos a agrupar de dos en dos
    '''
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)


def fig_print_zones(fig, changes_array):
    '''
    Dibuja un rectángulo en la figura que se le pasa y en las posiciones indicadas.
    Parameters:
        fig : figura en la que se van a añadir los rectángulos
        golpeos_array : lista con los valores de x en los que se considera que hay eventos
    '''
    for num1, num2 in pairwise(changes_array):
        fig.add_vrect(x0=num1, x1=num2, fillcolor="green", opacity=0.25, line_width=0)


def fig_print_golpeos(fig, changes_array, x: pd.Series, duracion=120):
    '''
    Toma una figura y la lista de inicios y finales de eventos y añade rectángulos verdes
    en las zonas con eventos, siempre que estos duren más que la duración inidicada.
    Parameters:
        fig : figura en la que se van a añadir los rectángulos
        changes_array : lista de inicios y finales de eventos
        x : serie de pandas que contiene los valores del eje horizontal
        duracion : mínima diferencia entre inicio y final de evento para ser representado.
    Returns:
        golpeos_array : lista con los valores de x en los que se considera que hay eventos
    '''
    golpeos_array = []
    for num1, num2 in pairwise(changes_array):
        if num2 - num1 > duracion:
            golpeos_array.append(x.loc[num1])
            golpeos_array.append(x.loc[num2])
    fig_print_zones(fig, golpeos_array)
    return golpeos_array


def detecta_golpeos(df, medida, prueba_playerID, path_save, window=10, ratio=1_000, hyst_up=10, duracion=120):
    '''
    Grafica la medida indicada del dataframe que se pasa, aplica funciones de detección de movimiento y se muestran los
    resultados obtenidos. El usuario decide si repetir la detección cambiando algún parámetro o si no repetir. Si no
    repite, el usuario tiene la posibilidad de eliminar alguna de las detecciones manualmente. Finalmente, se devuelve
    una lista con los eventos que el usuario ha mantenido.
    Parameters:
        ...
    Returns:
        golpeos_array : lista con los inicios y finales de los eventos detectados
        golpeos_eliminados : lista con los inicios y finales de los eventos eliminados
    '''


    signal = df[medida]
    repetir_deteccion = 1
    lista_comandos = []
    while repetir_deteccion:
        title = f"Jugador {prueba_playerID.split('_')[1]} {prueba_playerID.split('_')[0]} {medida} - window=" \
                f"{window}, ratio={ratio}, hyst_up={hyst_up}, duracion={duracion}"
        lista_comandos.append([window, ratio, hyst_up, duracion])
        flat_data_id, signal_diff_prod_w, up_th = find_flat_p(signal, window=window, ratio=ratio)
        flat_data_id, changes_array = hystheresis_threshold(signal_diff_prod_w, up_th, hyst_up=hyst_up)

        metrica_deteccion = signal_diff_prod_w #(signal-up_th)/(up_th-signal.mean())
        # (acc_y - threshold) / (threshold - acc_y_mean)

        df['movimiento'] = flat_data_id
        # df['timestamp_s'] = pd.to_datetime(df['timestamp'], unit='s')
        # df['tiempo_relativo'] = [*range(len(df))]
        df['tiempo_relativo'] = [x * 0.02 / 60 for x in [*range(len(df))]]
        x = [k for k in [*range(len(df))]] #df['tiempo_relativo']
        not_clean = 1
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=x, y=signal, name='Datos raw'))
        factor = 1 if 'acc' in medida else 100
        fig.add_trace(go.Scatter(x=x, y=df['movimiento']*factor, name='Histéresis'))
        golpeos_array = fig_print_golpeos(fig, changes_array, x=pd.Series(x), duracion=duracion)
        fig.update_layout(title_text=title, title_x=0.5)
        plot(fig)
        respuesta = ''
        # respuesta = input('Pulsa r si quieres cambiar parámetros y repetir la detección: \r\n')
        if respuesta == 'r':
            lista_comandos.append(respuesta)
            repetir_deteccion = 1
            respuesta = input(
                f'Introduce entre comas los nuevos valores de window ({window}), ratio ({ratio}), hyst_up ({hyst_up}) y duracion ({duracion}):\r\n')
            if len(respuesta) > 0:
                valores = [x for x in respuesta.split(sep=',')]
                window = int(valores[0])
                ratio = float(valores[1])
                hyst_up = int(valores[2])
                duracion = int(valores[3])
        else:
            valores = [window, ratio, hyst_up, duracion]
            repetir_deteccion = 0
            golpeos_eliminados = []
            while not_clean:
                # respuesta = input('Indica entre comas el número de los eventos erróneos (empieza en 1) y pulsa enter: \r\n')
                if len(respuesta) > 0:
                    lista_comandos.append(respuesta)
                    errores = [int(x) for x in respuesta.split(sep=',')]
                    errores.sort()
                    for error in reversed(errores):
                        golpeos_eliminados.append(golpeos_array.pop(error * 2 - 1))
                        golpeos_eliminados.append(golpeos_array.pop(error * 2 - 2))
                    fig = go.Figure()
                    fig.add_trace(go.Scatter(x=x, y=signal))
                    fig.add_trace(go.Scatter(x=x, y=df['movimiento']))
                    fig_print_zones(fig, golpeos_array)
                    fig.update_layout(title_text=title, title_x=0.5)
                    plot(fig)
                else:
                    not_clean = 0

    if not os.path.isdir(path_save):
        os.mkdir(path_save)
    fig.write_html('/'.join([path_save,f"{prueba_playerID.split('_')[1]}_{prueba_playerID.split('_')[0]}_window"
                                       f"={window}_ratio={ratio}_hyst_up={hyst_up}_duracion={duracion}.html"]))
    with open('/'.join([path_save,'parametros.txt']), 'w') as f:
        for linea in lista_comandos:
            f.writelines(f"{linea}\r\n")

    return golpeos_array, golpeos_eliminados, valores, metrica_deteccion, up_th


def segmenta_DF_entero_en_eventos(golpeos_array, df, path, name_event):
    '''
    Segmenta el df para guardar los archivos correspondientes a cada evento. Los eventos varían según el name_event:
        'CambiosDireccion'
        'ChutePieDerecho'
        'ChutePieIzquierdo'
        'PaseConBalonPieDerecho'
        'PaseConBalonPieIzquierdo'
        'PaseSinBalonPieDerecho'
        'PaseSinBalonPieIzquierdo'
    Parameters:
        golpeos_array : lista con los inicios y finales de los eventos detectados
        df : dataframe con los datos a segmentar
        path : ruta para guardar los archivos
        name_event : nombre del evento
    '''
    path = path + '/' + name_event
    isExist = os.path.exists(path)
    if not isExist:
        os.makedirs(path)
    def extrae_y_guarda(inicio,fin,path_save,nombre_segmento,numero_evento):
        df_temp = df.loc[inicio:fin, :].reset_index(drop=True)
        df_temp.to_csv(path_save + f"/{nombre_segmento}/{df_temp['foot'].loc[0]}_evento{numero_evento}.csv", index=False)

    if name_event[:5] == 'Chute':
        nombre_segmentos = ['Golpeo', 'Pausa', 'Marcha']
        for nombre in nombre_segmentos:
            isExist = os.path.exists(path + '/' + nombre)
            if not isExist:
                os.makedirs(path + '/' + nombre)
        for num_evento, (ini_golpeo, fin_golpeo) in enumerate(pairwise(golpeos_array)):
            index_inicio = df['tiempo_relativo'].tolist().index(ini_golpeo)
            index_fin = df['tiempo_relativo'].tolist().index(fin_golpeo)

            ini_evento = index_inicio #(index_fin + index_inicio) // 2 - 75
            fin_evento = index_fin #75 + (index_fin + index_inicio) // 2
            extrae_y_guarda(ini_evento,fin_evento,path,'Golpeo',num_evento)
            # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
            # df_temp.to_csv(path + f"/Golpeo/{df_temp['foot'].loc[0]}_evento{num_evento}.csv", index=False)

            # ini_evento = index_fin
            # fin_evento = ini_evento + 150
            # extrae_y_guarda(ini_evento, fin_evento, path + f"/Pausa/{df_temp['foot'].loc[0]}_evento{num_evento}.csv")
            # # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
            # # df_temp.to_csv(path + f"/Pausa/{df_temp['foot'].loc[0]}_evento{num_evento}.csv", index=False)

            if num_evento != 0:
                # Busco el primer evento de marcha después de la pausa post golpeo
                for cont in range(fin_evento_ant, len(df), 1):
                    if df['movimiento'].loc[cont]:
                        ini_evento = cont
                        break
                extrae_y_guarda(fin_evento_ant, ini_evento, path, 'Pausa', num_evento)
                # fin_evento = ini_evento + 150
                # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
                # df_temp.to_csv(path + f"/Marcha1/{df_temp['foot'].loc[0]}_evento{num_evento - 1}.csv", index=False)
                for cont in range(index_inicio - 1, ini_evento, -1):
                    if df['movimiento'].loc[cont]:
                        fin_evento = cont
                        break
                # ini_evento = fin_evento - 150
                extrae_y_guarda(ini_evento, fin_evento, path, 'Marcha', num_evento)

                # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
                # df_temp.to_csv(path + f"/Marcha2/{df_temp['foot'].loc[0]}_evento{num_evento - 1}.csv", index=False)
            fin_evento_ant = index_fin

    if name_event == 'CambiosDeDireccion':
        nombre_segmentos = ['Cambio_con_dch', 'Cambio_con_izq', 'Marcha']
        for nombre in nombre_segmentos:
            isExist = os.path.exists(path + '/' + nombre)
            if not isExist:
                os.makedirs(path + '/' + nombre)
        for num_evento, (ini_movimiento, fin_movimiento) in enumerate(pairwise(golpeos_array)):
            index_inicio = df['tiempo_relativo'].tolist().index(ini_movimiento)
            index_fin = df['tiempo_relativo'].tolist().index(fin_movimiento)

            ini_evento = index_inicio
            fin_evento = (index_fin - index_inicio) // 2 + index_inicio
            extrae_y_guarda(ini_evento,fin_evento,path,'Cambio_con_dch',num_evento)
            # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
            # df_temp.to_csv(path + f"/{nombre_segmentos[0]}/{df_temp['foot'].loc[0]}_evento{num_evento}.csv",
            #                index=False)

            ini_evento = fin_evento
            fin_evento = index_fin
            extrae_y_guarda(ini_evento,fin_evento,path,'Cambio_con_izq',num_evento)
            # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
            # df_temp.to_csv(path + f"/{nombre_segmentos[1]}/{df_temp['foot'].loc[0]}_evento{num_evento}.csv",
            #                index=False)

            if num_evento != 0:
                fin_evento = index_inicio
                ini_evento = fin_evento_ant
                extrae_y_guarda(ini_evento,fin_evento,path,'Marcha',num_evento)
                # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
                # df_temp.to_csv(path + f"/{nombre_segmentos[2]}/{df_temp['foot'].loc[0]}_evento{num_evento - 1}.csv",
                #                index=False)
            fin_evento_ant = index_fin

    if name_event[:4] == 'Pase':
        nombre_segmentos = ['Pase']
        for nombre in nombre_segmentos:
            isExist = os.path.exists(path + '/' + nombre)
            if not isExist:
                os.makedirs(path + '/' + nombre)
        for num_evento, (ini_movimiento, fin_movimiento) in enumerate(pairwise(golpeos_array)):
            index_inicio = df['tiempo_relativo'].tolist().index(ini_movimiento)
            index_fin = df['tiempo_relativo'].tolist().index(fin_movimiento)
            punto_medio = (index_fin - index_inicio) // 2 + index_inicio

            ini_evento = punto_medio - 75
            fin_evento = punto_medio + 75
            extrae_y_guarda(ini_evento,fin_evento,path,'Pase',num_evento)
            # df_temp = df.loc[ini_evento:fin_evento, :].reset_index(drop=True)
            # df_temp.to_csv(path + f"/{nombre_segmentos[0]}/{df_temp['foot'].loc[0]}_evento{num_evento}.csv",
            #                index=False)


def saveDFetiquetado(df_save, eventos_detectados, file_path):
    '''
    Guarda un dataframe en csv añadiendo una columna que indica los inicios y finales de los eventos.
    Parameters:
        df_save : dataframe que se quiere guardar
        eventos_detectados : lista de inicios y finales de eventos
        file_path : ruta en la que se guarda el .csv
    '''
    df_save['evento'] = [0] * len(df_save)
    for tiempo_evento in eventos_detectados:
        index_evento = df_save['tiempo_relativo'].tolist().index(tiempo_evento)
        df_save['evento'].loc[index_evento] = 1
    df_save.to_csv(file_path)


def open_printCSV(file_path, medida):
    '''
    Abre un archivo csv y grafica la medida indicada
    Parameters:
        file_path : ruta al archivo que queremos abrir
        medida : medida que se quiere graficar
    Returns:
        df_opened : devuelve el archivo abierto como dataframe
    '''
    df_opened = pd.read_csv(file_path)
    mask = (df_opened['evento'] == 1).tolist()
    rectangulos = [i for i, x in enumerate(mask) if x == True]
    fig = go.Figure()
    df = pd.DataFrame()
    df['sum1'] = [x for x in [*range(len(df_opened))]]
    x = df['sum1']
    fig.add_trace(go.Scatter(x=x, y=df_opened[medida]))
    fig_print_zones(fig, rectangulos)
    plot(fig)
    return df_opened


def resume_analysis(file_path, show=0, medida=None):
    '''
    Abre un archivo csv generado tras detectar golpeos y devuelve el archivo como dataframe
    y los límites de los eventos detectados. Si se indica, grafica la medida indicada marcando
    los eventos.
    Parameters:
        file_path : ruta al archivo que queremos abrir
        show : si es 1 permite graficar la medida deseada
        medida : medida que se quiere graficar
    Returns:
        df_opened : devuelve el archivo abierto como dataframe
        rectangulos : lista con los inicios y finales de los eventos
    '''
    df_opened = pd.read_csv(file_path)
    rectangulos = [index for index, evento in enumerate(df_opened['evento'].tolist()) if (evento)]
    if show:
        fig = go.Figure()
        df = pd.DataFrame()
        df['sum1'] = [x for x in [*range(len(df_opened))]]
        x = df['sum1']
        fig.add_trace(go.Scatter(x=x, y=df_opened[medida]))
        fig_print_zones(fig, rectangulos)
        plot(fig)
    return df_opened, rectangulos

# %%
# Representa todas las componentes del segmento introducido
def print_segmento(path_segmento):
    df_segmento = pd.read_csv(path_segmento)
    row_titles = ['acx', 'acy', 'acz']
    n_plots = len(row_titles)
    carpetas = path_segmento.split(sep='/')
    fig = make_subplots(n_plots, 1, shared_xaxes=True, x_title='Time (s)', row_titles=row_titles, vertical_spacing=0.02,
                        row_heights=[1 / n_plots] * n_plots,
                        column_titles=[f"{carpetas[-3]} - {carpetas[-2]} - {carpetas[-1]}", '', ''])

    x = df_segmento.tiempo_relativo

    fig.add_trace(go.Scatter(x=x, y=df_segmento.acx), row=1, col=1)
    fig.add_trace(go.Scatter(x=x, y=df_segmento.acy), row=2, col=1)
    fig.add_trace(go.Scatter(x=x, y=df_segmento.acz), row=3, col=1)
    fig.update_layout()
    fig.show()
def formato_edge_impulse(path_raiz):
    '''Genera una carpeta llamada _datos_formato_edge_impulse y posteriormente va entrando en las carpetas de
    datos_segmentados de cada carpeta de jugador, guardando los datos en el formato timestamp,accX,accY,
    accZ y poniendo de nombre el nombre de la prueba y del evento y un ID de 4 dígitos, los 2 primeros serán del ID del
    jugador y
    los 2 últimos del ID de la muestra.'''
    names_columns_edge_impulse = ['timestamp','accX','accY','accZ']
    names_columns_df = ['tiempo_relativo','acx','gyry','acz']

    lista_ID_jugadores = os.listdir(PATH_CARPETA_RAIZ)
    path_save_edge_impulse = f"{path_raiz}/_datos_formato_edge_impulse"
    if not os.path.isdir(path_save_edge_impulse):
        os.mkdir(path_save_edge_impulse)
    for ID_jugador in lista_ID_jugadores:
        path_segmentados = f"Datos_jugadores/{ID_jugador}/_datos_segmentados"
        lista_pruebas = os.listdir(path_segmentados)
        for prueba in lista_pruebas:
            path_prueba = '/'.join([path_segmentados,prueba])
            tipo_eventos = os.listdir(path_prueba)
            for evento in tipo_eventos:
                path_evento = '/'.join([path_prueba,evento])
                lista_archivos = os.listdir(path_evento)
                for archivo in lista_archivos:
                    df_archivo = pd.read_csv('/'.join([path_evento, archivo]))
                    df_edge_impulse = pd.DataFrame(columns=names_columns_edge_impulse)
                    for column_df, column_edge_impulse in zip(names_columns_df, names_columns_edge_impulse):
                        if column_edge_impulse == 'timestamp':
                            df_edge_impulse[column_edge_impulse] = (df_archivo[column_df]                                                                                       - \
                                df_archivo[column_df][0])#*60
                        elif column_df == 'gyry':
                            df_edge_impulse[column_edge_impulse] = df_archivo[column_df]/100

                        else:
                            df_edge_impulse[column_edge_impulse] = df_archivo[column_df]
                    ID_sample = (archivo.lstrip('right_evento')).split('.')[0] if 'right' in archivo else (
                            archivo.lstrip('left_evento')).split('.')[0]
                    df_edge_impulse.to_csv('/'.join([path_save_edge_impulse,f"{prueba}_{evento}.{ID_jugador}"
                                                                            f"{ID_sample if len(ID_sample)>=2 else '0'+ID_sample}.csv"]),index=False)
#                 todo: quedarme con las 3 columnas correspondientes a timestamp,accX,accY,accZ; cambiar los nombres
#                  de las columnas por los de estas 4 y guardar el df como csv en la carpeta que se haga con el
#                  nombre de archivo que se ha decidido.

def segmentacion_manual(path_imu, medida, test_name, player_ID, path_save=None, row_titles =['acc_x', 'gyro_y', 'acc_z']):
    df_pieDch, df_pieIzq = carga_datos(path_imu)
    if test_name in ['PaseConBalonPieDerecho','PaseSinBalonPieDerecho','ChutePieDerecho']:
        list_data=[df_pieIzq]
    elif test_name in ['PaseConBalonPieIzquierdo','PaseSinBalonPieIzquierdo','ChutePieIzquierdo']:
        list_data=[df_pieIzq]
    if test_name == 'CambiosDeDireccion':
        list_data=[df_pieIzq,df_pieIzq]
        event_duration = 1500
    else:
        event_duration = 400
    for data in list_data:
        df_limites = pd.DataFrame(columns=['inicio','final'])
        indexes = [*range(len(data))]#datetime.fromtimestamp(x for x in data['timestamp'].tolist()).time()#
          # ['acx', 'acy', 'acz']# ['acx', 'acy', 'acz']
        n_plots = len(row_titles)
        fig = make_subplots(n_plots, 1, shared_xaxes=True, x_title='Time (s)', row_titles=row_titles,
                            vertical_spacing=0.02, row_heights=[1 / n_plots] * n_plots)
        for i,trace_title in enumerate(row_titles):
            fig.add_trace(go.Scatter(x=indexes, y=data[trace_title]), row=i+1, col=1)
            # fig.add_trace(go.Scatter(x=indexes, y=data[row_titles[1]]), row=2, col=1)
            # fig.add_trace(go.Scatter(x=indexes, y=data[row_titles[2]]), row=3, col=1)

        title = f'{test_name} Player: {player_ID} ' + ' ' + data['foot'].loc[0] + ' ' + medida
        fig.update_layout(title_text=title, title_x=0.5)
        if path_save is not None:
            if not os.path.isdir(path_save):
                os.mkdir(path_save)
            path_save_1 = '/'.join([path_save, test_name])
            # if not os.path.isdir(path_save_1):
            #     os.mkdir(path_save_1)
            # fig.write_html('/'.join([path_save_1, f"/{medida}_{data['foot'].loc[0]}.html"]))
        plot(fig)
        index_ini = 0
        index_fin = event_duration
        mostrar_otro_evento = 1
        while index_ini != 'x':
            print(mostrar_otro_evento)
            mostrar_otro_evento += 1
            adjusting=1
            while adjusting:
                fig.data = []
                for i, trace_title in enumerate(row_titles):
                    fig.add_trace(go.Scatter(x=indexes[index_ini:index_fin], y=data[trace_title].loc[
                                                                               index_ini:index_fin]), row=i+1, col=1)
                # fig.add_trace(go.Scatter(x=indexes[index_ini:index_fin], y=data[row_titles[1]].loc[index_ini:index_fin]), row=2, col=1)
                # fig.add_trace(go.Scatter(x=indexes[index_ini:index_fin], y=data[row_titles[2]].loc[index_ini:index_fin]), row=3, col=1)
                plot(fig)
                respuesta = ''
                while not ',' in respuesta:
                    respuesta = input('Indica el inicio y el final del evento (o pulsa enter para ajustar el inicio): \r\n')
                    if len(respuesta) == 0:
                        break
                if len(respuesta) > 0:
                    adjusting = 0
                    list = [int(x) for x in respuesta.split(sep=',')]
                    df_limites.loc[len(df_limites)] = list
                    # mostrar_otro_evento = input('Pulsa x si quieres dejar de segmentar:')
                    index_ini_new = ','
                    while ',' in index_ini_new :
                        index_ini_new = input('Indica el inicio del siguiente gráfico o pulsa x para parar: \r\n')
                    if index_ini_new != 'x':
                        index_ini = int(index_ini_new)
                        index_fin = index_ini + event_duration
                    else:
                        index_ini = index_ini_new
                else:
                    index_ini = int(input('Ajusta el inicio del gráfico: \r\n'))
                    index_fin = index_ini + event_duration
        if test_name == 'CambiosDeDireccion':
            df_limites.to_csv('/'.join([path_save, test_name+'_'+data['foot'].loc[0]+'.csv']))
        else:
            df_limites.to_csv('/'.join([path_save, test_name+'.csv']))
def grafica_por_demanda():
    while True:
        jugador = questionary.select(
            "Selecciona Jugador:",
            choices=lista_ID_jugadores
        ).ask()  # returns value of selection
        path_jugador = '/'.join([PATH_CARPETA_RAIZ, jugador, PATH_PARQUET])

        lista_archivos_pruebas = os.listdir(path_jugador)
        prueba_seleccionada = questionary.select(
            "Selecciona Prueba:",
            choices=lista_archivos_pruebas
        ).ask()  # returns value of selection
        path_prueba = '/'.join([path_jugador, prueba_seleccionada])

        path_limites = '/'.join([PATH_CARPETA_RAIZ, jugador, '_excel_inicio_fin'])
        lista_archivos_limites = os.listdir(path_limites)
        limites = []
        if 'left' in lista_archivos_limites[1]:
            limites.append(lista_archivos_limites[1])
            limites.append(lista_archivos_limites[0])
        else:
            limites.append(lista_archivos_limites[0])
            limites.append(lista_archivos_limites[1])

        df_pieDch, df_pieIzq = carga_datos(path_prueba)
        list_data = [df_pieDch, df_pieIzq]
        row_titles = questionary.checkbox(
            "Selecciona los datos que quieras analizar:",
            choices=df_pieDch.columns.tolist()
        ).ask()
        for data, limites_file in zip(list_data,limites):
            indexes = [*range(len(data))]
            n_plots = len(row_titles)
            fig = make_subplots(n_plots, 1, shared_xaxes=True, x_title='Time (s)', row_titles=row_titles,
                                vertical_spacing=0.02, row_heights=[1 / n_plots] * n_plots)
            for i, trace_title in enumerate(row_titles):
                max_ventana = data[trace_title].rolling(20, center=True, min_periods=1).max()
                min_ventana = data[trace_title].rolling(20, center=True, min_periods=1).min()
                varianza_ventana = data[trace_title].rolling(20, center=True, min_periods=1).var()

                fig.add_trace(go.Scatter(x=indexes, y=data[trace_title], name=trace_title), row=i + 1, col=1)
                # fig.add_trace(go.Scatter(x=indexes, y=data[trace_title].rolling(20, center=True, min_periods=1).mean(),
                #                          name= trace_title+' mean'), row=i + 1, col=1)
                fig.add_trace(go.Scatter(x=indexes, y=max_ventana.diff(),
                                         name= trace_title+' max'), row=i + 1, col=1)
                fig.add_trace(go.Scatter(x=indexes, y=min_ventana.diff(),
                                         name= trace_title+' min'), row=i + 1, col=1)

                fig.add_trace(go.Scatter(x=indexes, y=varianza_ventana.diff(),
                                         name= trace_title+' var'), row=i + 1, col=1)
                # fig.add_trace(go.Scatter(x=indexes, y=data[trace_title].rolling(20, center=True,
                #                                                                 min_periods=1).median(),
                #                          name= trace_title+' median'), row=i + 1, col=1)


            if 'CambiosDeDireccion' in prueba_seleccionada:
                df_limites = pd.read_csv('/'.join([path_limites, limites_file]), sep=',')
                for i in [*range(len(df_limites))]:
                    fig.add_vrect(x0=df_limites['inicio'].loc[i], x1=df_limites['final'].loc[i])

            title = f"{prueba_seleccionada.split('_')[0]} Player: {jugador} " + ' ' + data['foot'].loc[0]
            fig.update_layout(title_text=title, title_x=0.5)
            plot(fig)
            save = questionary.select(
                "Quieres guardarlo?:",
                choices=['Sí','No']
            ).ask()
            if save == 'Sí':
                fig = make_subplots(n_plots, 1, shared_xaxes=True, x_title='Time (s)', row_titles=row_titles,
                                    vertical_spacing=0.02, row_heights=[1 / n_plots] * n_plots)
                for i, trace_title in enumerate(row_titles):
                    fig.add_trace(go.Scatter(x=indexes, y=data[trace_title]), row=i + 1, col=1)
                for i in [*range(len(df_limites))]:
                    fig.add_vrect(x0=df_limites['inicio'].loc[i], x1=df_limites['final'].loc[i])
                title = f"{prueba_seleccionada.split('.')[0]}"+data['foot'].loc[0]
                for variable in row_titles:
                    title = f"{title}_{variable}"
                fig.update_layout(title_text=title, title_x=0.5)
                path_save = 'Analisis_variables_cambios_direccion/'+ title + '.html'
                # if path_save is not None:
                #     if not os.path.isdir(path_save):
                #         os.mkdir(path_save)
                #     path_save_1 = '/'.join([path_save, test_name])
                    # if not os.path.isdir(path_save_1):
                    #     os.mkdir(path_save_1)
                    # fig.write_html('/'.join([path_save_1, f"/{medida}_{data['foot'].loc[0]}.html"]))
                fig.write_html(path_save)
# %%
if __name__ == "__main__":
    SOLO_VISUALIZAR = 1
    DETECTAR_EVENTOS = 1
    PATH_CARPETA_RAIZ = 'Datos_jugadores'
    lista_ID_jugadores = os.listdir(PATH_CARPETA_RAIZ)
    PATH_CSV = 'datos IMU'
    PATH_PARQUET = 'parquet'
    PATH_GRAFICAS = '_graficas'
    PATH_SEGMENTADOS = '_datos_segmentados'
    PATH_LIMITES = '_excel_inicio_fin'

    test_names = ['CambiosDeDireccion',
                  'ChutePieDerecho',
                  'ChutePieIzquierdo',
                  'PaseConBalonPieDerecho',
                  'PaseConBalonPieIzquierdo',
                  'PaseSinBalonPieDerecho',
                  'PaseSinBalonPieIzquierdo']


    # lista_imu_files = os.listdir('Datos RETOS_07092022/parquet/02/')
    # lista_crono_files = os.listdir('Datos RETOS_07092022/cronos/01/')
    # print_segmento('Datos_jugadores/01/_datos_segmentados/CambiosDeDireccion/Cambio_a_dch/right_evento0.csv')

    # formato_edge_impulse(PATH_CARPETA_RAIZ)

    # grafica_por_demanda()

    jugadores_seleccionados = questionary.checkbox(
        "Selecciona los JUGADORES que quieras analizar:",
        choices=lista_ID_jugadores
        ).ask()  # returns value of selection



    for jugador in jugadores_seleccionados:
        tipo_de_archivo = questionary.checkbox(
            "Son .CSV o .PARQUET?",
            choices=['csv', 'parquet']
        ).ask()  # returns value of selection
        if tipo_de_archivo[0]=='parquet':
            if not os.path.exists('/'.join([PATH_CARPETA_RAIZ,jugador,PATH_PARQUET])):
                tipo_de_archivo[0]='csv'
        path_jugador = '/'.join([PATH_CARPETA_RAIZ,jugador,(PATH_CSV if tipo_de_archivo[0]=='csv' else PATH_PARQUET)])

        lista_archivos_pruebas = os.listdir(path_jugador)
        pruebas_seleccionadas = questionary.checkbox(
            "Selecciona los TESTS que quieras analizar:",
            choices=lista_archivos_pruebas
        ).ask()  # returns value of selection

        for prueba in pruebas_seleccionadas:
            # path_prueba = path_jugador + '/' + any(prueba in archivo for archivo in lista_archivos_pruebas)
            path_prueba_file = '/'.join([path_jugador, prueba])
            prueba_name = prueba.split('_')[0]
            show_en = questionary.confirm("Quieres representar los datos?")
            if False:
                path_save = '/'.join([PATH_CARPETA_RAIZ,jugador,PATH_GRAFICAS])
                path_save_limites = '/'.join([PATH_CARPETA_RAIZ,jugador,PATH_LIMITES])
                segmentacion_manual(path_prueba_file, 'acc', prueba_name, jugador, path_save_limites)
                print_all_data(path_prueba_file, 'acc', prueba_name, jugador, path_save)
                print_all_data(path_prueba_file, 'gyro', prueba_name, jugador, path_save)

        hacer_segmentacion_auto=1
        if hacer_segmentacion_auto:
                if prueba_name == 'CambiosDeDireccion':
                    valores_para_deteccion = ['dch', 'acx', 10, 1000, 10, 100]
                elif prueba_name == 'PaseConBalonPieDerecho':
                    valores_para_deteccion = ['dch', 'acz', 10, 1, 4, 10]
                elif prueba_name == 'PaseConBalonPieIzquierdo':
                    valores_para_deteccion = ['izq', 'acz', 10, 1, 4, 10]
                elif prueba_name == 'PaseSinBalonPieDerecho':
                    # continue
                    valores_para_deteccion = ['dch', 'acz', 10, 1, 4, 10]
                elif prueba_name == 'PaseSinBalonPieIzquierdo':
                    # continue
                    valores_para_deteccion = ['izq', 'acz', 10, 1, 4, 10]
                elif prueba_name == 'ChutePieDerecho':
                    valores_para_deteccion = ['dch', 'gyro_y', 20, 0.4, 10, 5]
                elif prueba_name == 'ChutePieIzquierdo':
                    valores_para_deteccion = ['izq', 'gyro_y', 20, 0.3, 10, 5]#['izq', 'acx', 10, 1000, 10, 100]

                df_dch, df_izq = carga_datos(path_prueba_file)
                path_save = '/'.join([PATH_CARPETA_RAIZ, jugador, PATH_GRAFICAS,prueba_name])
                title = prueba_name + '_' + valores_para_deteccion[1] + f'_pie_{valores_para_deteccion[0]}'
                eventos_detectados, eventos_eliminados, valores_para_deteccion_updt = detecta_golpeos(
                    df_dch if valores_para_deteccion[0] == 'dch' else df_izq,
                    valores_para_deteccion[1],
                    prueba_name+'_'+jugador,
                    path_save,
                    window=valores_para_deteccion[2],
                    ratio=valores_para_deteccion[3],
                    hyst_up=valores_para_deteccion[4],
                    duracion=valores_para_deteccion[5])
                valores_para_deteccion_updt = [valores_para_deteccion[0]] + [valores_para_deteccion[1]] + \
                                              valores_para_deteccion_updt
                df_save = df_dch.copy() if valores_para_deteccion_updt[0] == 'dch' else df_izq.copy()
                file_path = '/'.join([path_save,f"{title}_window={valores_para_deteccion_updt[2]}_ratio={valores_para_deteccion_updt[3]}_hyst_up={valores_para_deteccion_updt[4]}_duracion={valores_para_deteccion_updt[5]}.csv"])
                # file_path = f'{path_save}\{player_ID}\{test_name}\{title}_window={valores_para_deteccion[2]}_ratio={valores_para_deteccion[3]}_hyst_up={valores_para_deteccion[4]}_duracion={valores_para_deteccion[5]}.csv'
                # file_path = f'{path_save}\{player_ID}\{test_name}\{title}_window={valores_para_deteccion[2]}_ratio={valores_para_deteccion[3]}_hyst_up={valores_para_deteccion[4]}_duracion={valores_para_deteccion[5]}.csv'
                saveDFetiquetado(df_save, eventos_detectados, file_path)
                path_save_seg = '/'.join([PATH_CARPETA_RAIZ, jugador, PATH_SEGMENTADOS])
                if not os.path.isdir(path_save_seg):
                    os.mkdir(path_save_seg)
                segmenta_DF_entero_en_eventos(eventos_detectados, df_dch if valores_para_deteccion_updt[0] == 'dch' else df_izq,
                                              path_save_seg, prueba_name)

        # for test_name in test_names:
        #     # separados = crono_file_path.split(sep='_')
        #
        #     for path in lista_imu_files:
        #         if test_name in path:
        #             parquet_file_path = path
        #             ID_ini = path.find(test_name) + len(test_name) + 1
        #             player_ID = parquet_file_path[ID_ini:ID_ini + 2]
        #             break
        #     path_imu = f'Datos RETOS_07092022/parquet/{player_ID}/' + parquet_file_path
        #     # path_crono = 'Datos RETOS_07092022/cronos/01/' + crono_file_path
        #     if test_name == 'CambiosDireccion':
        #         valores_para_deteccion = ['dch', 'acc_x', 10, 1000, 10, 100]
        #     elif test_name == 'PaseConBalonPieDerecho':
        #         valores_para_deteccion = ['dch', 'acc_z', 10, 1, 4, 10]
        #     elif test_name == 'PaseConBalonPieIzquierdo':
        #         valores_para_deteccion = ['izq', 'acc_z', 10, 1, 4, 10]
        #     elif test_name == 'PaseSinBalonPieDerecho':
        #         continue
        #         valores_para_deteccion = ['dch', 'acc_z', 10, 1, 4, 10]
        #     elif test_name == 'PaseSinBalonPieIzquierdo':
        #         continue
        #         valores_para_deteccion = ['izq', 'acc_z', 10, 1, 4, 10]
        #     elif test_name == 'ChutePieDerecho':
        #         valores_para_deteccion = ['dch', 'acc_x', 10, 1000, 10, 100]
        #     elif test_name == 'ChutePieIzquierdo':
        #         valores_para_deteccion = ['izq', 'acc_x', 10, 1000, 10, 100]
        #
        #     path_save = 'D:\Pictures\deteccion eventos futbol'
        #     if SOLO_VISUALIZAR:
        #         print_all_data(path_imu, 'acc', test_name, player_ID, path_save)
        #         print_all_data(path_imu, 'gyro', test_name, player_ID, path_save)
        #     if DETECTAR_EVENTOS:
        #         df_dch, df_izq = carga_datos(path_imu)
        #         title = test_name + '_' + valores_para_deteccion[1] + f'_pie_{valores_para_deteccion[0]}'
        #         eventos_detectados, eventos_eliminados = detecta_golpeos(
        #             df_dch if valores_para_deteccion[0] == 'dch' else df_izq,
        #             valores_para_deteccion[1],
        #             title,
        #             path_save,
        #             window=valores_para_deteccion[2],
        #             ratio=valores_para_deteccion[3],
        #             hyst_up=valores_para_deteccion[4],
        #             duracion=valores_para_deteccion[5])
        #
        #         df_save = df_dch.copy() if valores_para_deteccion[0] == 'dch' else df_izq.copy()
        #         file_path = f'{path_save}\{player_ID}\{test_name}\{title}_window={valores_para_deteccion[2]}_ratio={valores_para_deteccion[3]}_hyst_up={valores_para_deteccion[4]}_duracion={valores_para_deteccion[5]}.csv'
        #         saveDFetiquetado(df_save, eventos_detectados, file_path)

    # %%

    df_dch, df_izq, events_position = carga_datos(path_imu, path_crono)
    pases_array1 = detecta_golpeos()

    # %%
    # CONFIGURACIÓN PARA PASES, PARA SEGMENTAR TOMAR LO DETECTADO COMO REFERENCIA Y AMPLIAR
    path_pases = r'D:\Documents\_GitLab\smartinsole_tool\data\session\2022.09.13T07.12.32Z_PaseConBalónPieDerecho_01_UNKNOWN.parquet.snappy'
    path_crono_pases = 'Datos RETOS_07092022/cronos/01/pase con balón drcha_01.csv'
    df_dch, df_izq, events_position = carga_datos(path_pases, path_crono_pases)
    pases_array1 = detecta_golpeos(df_dch, 'acc_z', 'Pases pierna derecha pie derecho -> acc_z', window=10, ratio=1,
                                   hyst_up=4, duracion=10)

    # %%
    path_pases = r'D:\Documents\_GitLab\smartinsole_tool\data\session\2022.09.13T07.12.32Z_PaseConBalónPieDerecho_01_UNKNOWN.parquet.snappy'
    path_crono_pases = 'Datos RETOS_07092022/cronos/01/pase con balón drcha_01.csv'

    path_imu_pro = r'D:\Documents\_GitLab\smartinsole_tool\data\session\2022.09.12T08.40.46Z_cambios_direccion_01_UNKNOWN.parquet.snappy'
    path_crono_pro = 'Datos RETOS_07092022/cronos/01/cambios dirección _01.csv'

    print_all_data(
        r'D:\\Documents\\_GitLab\\smartinsole_tool\\data\\session\\2022.09.12T08.40.46Z_cambios_direccion_01_UNKNOWN.parquet.snappy',
        r'Datos RETOS_07092022/cronos/01/cambios dirección _01.csv', 'gyro', parquet=1)

    # path_IMU = 'datos_podoactiva_220902/2022.09.02T10.03.56Z_Golpeo Balón Parado Pie Derecho_UNKNOWN.parquet.snappy'
    # path_crono = 'datos_podoactiva_220902/Datos crono/balón parado derecho.csv'
    path_IMU = r'D:\Documents\_GitLab\smartinsole_tool\data\session\2022.09.12T08.40.46Z_cambios_direccion_01_UNKNOWN.parquet.snappy'  # 'datos_podoactiva_220902/2022.09.02T10.04.15Z_Golpeo Balón Parado Pie Izquierdo_UNKNOWN.parquet.snappy'
    path_crono = 'Datos RETOS_07092022/cronos/01/cambios dirección _01.csv'  # 'datos_podoactiva_220902/Datos crono/cambios dirección.csv'
    path_save = 'datos_podoactiva_220902/datos_segmentados'

    # %%
    df_pie1, df_pie2, events_position = carga_datos(path_IMU, path_crono)
    golpeos_array1 = detecta_golpeos(df_pie1, 'acc_x', window=10, ratio=1_000, hyst_up=10, duracion=100)
    golpeos_array2 = detecta_golpeos(df_pie2, 'acc_x', window=10, ratio=1_000, hyst_up=10, duracion=100)
    segmenta_DF_entero_en_eventos(golpeos_array1, df_pie1, path_save, 'Chute_Izq')
    segmenta_DF_entero_en_eventos(golpeos_array2, df_pie2, path_save, 'Chute_Izq')
    # %%
    path_crono_pro = 'Datos RETOS_07092022/cronos/01/chute derecha_01.csv'
    path_imu_pro = 'Datos RETOS_07092022/datos IMU/01/ChutePieDerecho_01.csv'
    path_save_pro = 'Datos RETOS_07092022/datos_segmentados/01/'

    # %%
    # df_pie1_pro, df_pie2_pro = carga_datos_csv(path_imu_pro)
    # golpeos_array1_pro = detecta_golpeos(df_pie1_pro, 'acx', window=10, ratio=1_000, hyst_up=10, duracion=90)
    # golpeos_array2_pro = detecta_golpeos(df_pie2_pro, 'acx', window=10, ratio=1_000, hyst_up=10, duracion=90)
    # segmenta_golpeos_balon_parado(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'Chute_Dch')
    # segmenta_golpeos_balon_parado(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'Chute_Dch')

    path_save_pro = 'Datos RETOS_07092022/datos_segmentados/01/'
    dir_imu_path = 'Datos RETOS_07092022/datos IMU/01/'
    dir_crono_path = 'Datos RETOS_07092022/cronos/01/'
    lista_imu_files = os.listdir('Datos RETOS_07092022/datos IMU/01/')
    # lista_crono_files = os.listdir('Datos RETOS_07092022/cronos/01/')

    for imu_file in lista_imu_files:
        df_pie1_pro, df_pie2_pro = carga_datos_csv(dir_imu_path + imu_file)
        title = dir_imu_path + imu_file
        golpeos_array1_pro = detecta_golpeos(df_pie1_pro, 'acx', title + ' ' + df_pie1_pro['foot'].loc[0], window=10,
                                             ratio=1_000, hyst_up=10, duracion=90)
        golpeos_array2_pro = detecta_golpeos(df_pie2_pro, 'acx', title + ' ' + df_pie1_pro['foot'].loc[0], window=10,
                                             ratio=1_000, hyst_up=10, duracion=90)
        if imu_file[0:5] == 'Camb':
            segmenta_cambios_direccion(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'Cambios_direccion')
            segmenta_cambios_direccion(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'Cambios_direccion')
        elif imu_file[0:23] == 'PaseConBalónPieDerecho':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'PaseCon_Dch')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'PaseCon_Dch')
        elif imu_file[0:25] == 'PaseConBalónPieIzquierdo':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'PaseCon_Izq')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'PaseCon_Izq')
        elif imu_file[0:23] == 'PaseSinBalónPieDerecho':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'PaseSin_Dch')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'PaseSin_Dch')
        elif imu_file[0:25] == 'PaseSinBalónPieIzquierdo':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'PaseSin_Izq')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'PaseSin_Izq')
        elif imu_file[0:16] == 'ChutePieDerecho':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'Chute_Dch')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'Chute_Dch')
        elif imu_file[0:17] == 'ChutePieIzquierdo':
            segmenta_DF_entero_en_eventos(golpeos_array1_pro, df_pie1_pro, path_save_pro, 'Chute_Izq')
            segmenta_DF_entero_en_eventos(golpeos_array2_pro, df_pie2_pro, path_save_pro, 'Chute_Izq')


