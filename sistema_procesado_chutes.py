import pandas as pd
from segmentacion_datos import *
from PreProcessing import *
from Neural_models import *
from cod_test import *
import plotly.express as px

def identificar_chutes(row_jugador):

    dos_pies_df = cargar_df_sincronizado(row_jugador)

    # SE DETECTA EL PIE CON EL QUE SE HACE EL PASE
    valores_para_deteccion = ['acc_y_r', 20, 0.4, 8, 5]
    flat_data_id, signal_diff_prod_w, up_th_r = find_flat_p(dos_pies_df['acc_y_r'], window=valores_para_deteccion[1],
                                                            ratio=valores_para_deteccion[2])
    flat_data_id, signal_diff_prod_w, up_th_l = find_flat_p(dos_pies_df['acc_y_l'], window=valores_para_deteccion[1],
                                                            ratio=valores_para_deteccion[2])
    if up_th_l > up_th_r:
        valores_para_deteccion = ['acc_y_l', 20, 0.3, 10, 11]
        up_th = up_th_l
    else:
        valores_para_deteccion = ['acc_y_r', 22, 0.35, 10, 11]
        up_th = up_th_r

    #SE LOCALIZAN LOS GOLPEOS
    eventos_detectados, eventos_eliminados, valores_para_deteccion_updt, dos_pies_df['metrica'] = detecta_golpeos(
        dos_pies_df,
        valores_para_deteccion[0],
        row_jugador.prueba.split('_')[0] + '_' + row_jugador.jugador,
        row_jugador.path_save_sync,
        window=valores_para_deteccion[1],
        ratio=valores_para_deteccion[2],
        hyst_up=valores_para_deteccion[3],
        duracion=valores_para_deteccion[4])

    # SE AÑADE LA COLUMNA QUE INDICA SI SE CONSIDERA EVENTO O NO
    dos_pies_df['eventos'] = [np.nan] * len(dos_pies_df)
    dos_pies_df.loc[0, 'eventos'] = 0
    # Poner 1 si la los datos corresponden a un evento 0 si no es así
    for inicio, final in pairwise(eventos_detectados):
        dos_pies_df.loc[inicio, 'eventos'] = 1
        dos_pies_df.loc[final, 'eventos'] = 0
    dos_pies_df['eventos'].ffill(inplace=True)
    dos_pies_df['eventos'] = dos_pies_df['eventos'].astype(int)

    return dos_pies_df
def identificar_pases(row_jugador):

    dos_pies_df = cargar_df_sincronizado(row_jugador)

    # SE DETECTA EL PIE CON EL QUE SE HACE EL PASE
    valores_para_deteccion = ['acc_y_r', 20, 0.4, 8, 5]
    flat_data_id, signal_diff_prod_w, up_th_r = find_flat_p(dos_pies_df['acc_y_r'], window=valores_para_deteccion[1],
                                                            ratio=valores_para_deteccion[2])
    flat_data_id, signal_diff_prod_w, up_th_l = find_flat_p(dos_pies_df['acc_y_l'], window=valores_para_deteccion[1],
                                                            ratio=valores_para_deteccion[2])
    if up_th_l > up_th_r:
        valores_para_deteccion[0] = 'acc_y_l'
        up_th = up_th_l
    else:
        up_th = up_th_r
    #SI up_th es menor que 3 es sin balón, si es superior es con balón

    #SE LOCALIZAN LOS GOLPEOS
    eventos_detectados, eventos_eliminados, valores_para_deteccion_updt, dos_pies_df['metrica'] = detecta_golpeos(
        dos_pies_df,
        valores_para_deteccion[1],
        row_jugador.prueba.split('_')[0] + '_' + row_jugador.jugador,
        row_jugador.path_save_sync,
        window=valores_para_deteccion[2],
        ratio=valores_para_deteccion[3],
        hyst_up=valores_para_deteccion[4],
        duracion=valores_para_deteccion[5])

    #SE AÑADE LA COLUMNA QUE INDICA SI SE CONSIDERA EVENTO O NO
    dos_pies_df['eventos'] = [np.nan] * len(dos_pies_df)
    dos_pies_df.loc[0, 'eventos'] = 0
    # Poner 1 si la los datos corresponden a un evento 0 si no es así
    for inicio, final in pairwise(eventos_detectados):
        dos_pies_df.loc[inicio, 'eventos'] = 1
        dos_pies_df.loc[final, 'eventos'] = 0
    dos_pies_df['eventos'].ffill(inplace=True)
    dos_pies_df['eventos'] = dos_pies_df['eventos'].astype(int)

    return dos_pies_df

# %%
if __name__ == "__main__":
    #CARGAR UN DF parquet DE CHUTES
    manager = Paths_manager()
    manager.select_player_test()

    UP_TH_DF = pd.DataFrame()
    estadisticas_tuning = pd.DataFrame()
    #SINCRONIZAR AMBOS PIES
    for i, row_jugador in manager.paths_df.iterrows():
        dos_pies_df = cargar_df_sincronizado(row_jugador)
        # dos_pies_df = juntar_DFs_sincronizados(row_jugador)

    #ANÁLISIS CANTIDAD DE MOVIMIENTO
        if 'ChutePieDerecho' in row_jugador.prueba:
            valores_para_deteccion = ['dch', 'acc_y_r', 20, 0.4, 8, 5]#['dch', 'acc_y_r', 22, 0.35, 10, 11]#['dch',
            # 'gyro_y_r', 22, 0.35, 10, 11]
        elif 'ChutePieIzquierdo' in row_jugador.prueba:
            valores_para_deteccion = ['izq', 'acc_y_l', 20, 0.4, 8, 5]#['izq', 'acc_y_l', 20, 0.3, 10, 11] #['izq',
            # 'acx', 10, 1000, 10, 100]
        elif 'PaseConBalonPieDerecho' in row_jugador.prueba:
            # valores_para_deteccion = ['dch', 'gyro_y_r', 10, 1, 4, 10]
            valores_para_deteccion = ['dch', 'acc_y_r', 20, 0.4, 8, 5]
        elif 'PaseConBalonPieIzquierdo' in row_jugador.prueba:
            valores_para_deteccion = ['izq', 'acc_y_l', 20, 0.4, 8, 5]
        elif 'PaseSinBalonPieDerecho' in row_jugador.prueba:
            # valores_para_deteccion = ['dch', 'gyro_y_r', 10, 1, 4, 10]
            valores_para_deteccion = ['dch', 'acc_y_r', 20, 0.4, 8, 5]
        elif 'PaseSinBalonPieIzquierdo' in row_jugador.prueba:
            valores_para_deteccion = ['izq', 'acc_y_l', 20, 0.4, 8, 5]
        elif 'CambiosDeDireccion' in row_jugador.prueba:
            valores_para_deteccion = ['dch', 'gyro_y_r', 15, 1000, 10, 120]
        pass

        # for ventana in [5,10,15,20,25]:
        #     for radio in [0.3,0.4,0.5,0.75,1]:
        #         for histeresis in [2,4,6,8,10]:
        #             for duration in [5,8,10,11,12]:
        #                 valores_para_deteccion[2:] = [ventana,radio,histeresis,duration]
        # title = f"Jugador {row_jugador.jugador} {row_jugador.prueba.split('_')[0]} {valores_para_deteccion[1]} - " \
        #         f"window={valores_para_deteccion[2]}, ratio={valores_para_deteccion[3]}, " \
        #         f"hyst_up={valores_para_deteccion[4]}, duracion={valores_para_deteccion[5]}"
        eventos_detectados, eventos_eliminados, valores_para_deteccion_updt, dos_pies_df['metrica'], \
            up_th = detecta_golpeos(
            dos_pies_df,
            valores_para_deteccion[1],
            row_jugador.prueba.split('_')[0]+'_'+row_jugador.jugador,
            row_jugador.path_save_sync,
            window=valores_para_deteccion[2],
            ratio=valores_para_deteccion[3],
            hyst_up=valores_para_deteccion[4],
            duracion=valores_para_deteccion[5])

        dos_pies_df['eventos'] = [np.nan] * len(dos_pies_df)
        dos_pies_df.loc[0, 'eventos'] = 0
        # Poner 1 si la los datos corresponden a un evento 0 si no es así
        for inicio, final in pairwise(eventos_detectados):
            dos_pies_df.loc[inicio, 'eventos'] = 1
            dos_pies_df.loc[final, 'eventos'] = 0
        dos_pies_df['eventos'].ffill(inplace=True)
        dos_pies_df['eventos'] = dos_pies_df['eventos'].astype(int)



        # respuesta = input(f'Introduce el número total de picos presentes, el de picos no detectados y los no '
        #                   f'picos detectados:\r\n')
        # estadisticas_picos = [x for x in respuesta.split(sep=',')]

        new_row = pd.Series({'Player_ID': row_jugador.jugador,
                             'threshold': up_th,
                             # 'picos_reales': estadisticas_picos[0],
                             'detecciones': len(eventos_detectados)//2,
                             # 'picos_detectados': int(estadisticas_picos[0])-int(estadisticas_picos[1]),
                             # 'picos_no_detectados': estadisticas_picos[1],
                             # 'fallos': len(eventos_detectados)//2-(int(estadisticas_picos[0])-int(estadisticas_picos[1])),
                             # 'NO_picos_detectados': estadisticas_picos[2],
                             # 'picos_duplicados': (len(eventos_detectados)//2-(int(estadisticas_picos[0])-int(
                             #     estadisticas_picos[1])))-int(estadisticas_picos[2]),
                             'Prueba': row_jugador.prueba.split('_')[0],
                             'window': valores_para_deteccion_updt[0],
                             'ratio': valores_para_deteccion_updt[1],
                             'hyst_up': valores_para_deteccion_updt[2],
                             'duracion': valores_para_deteccion_updt[3]})

        UP_TH_DF = pd.concat([UP_TH_DF, new_row.to_frame().T], ignore_index=True)

    up_th_stats_df = pd.DataFrame()
    pruebas = ['ChutePieDerecho', 'ChutePieIzquierdo', 'PaseConBalonPieDerecho', 'PaseConBalonPieIzquierdo',
               'PaseSinBalonPieDerecho', 'PaseSinBalonPieIzquierdo']
    for prueba_in_list in pruebas:
        new_row_th = pd.Series({
            'Prueba': prueba_in_list,
            'Media': UP_TH_DF[UP_TH_DF['Prueba'] == prueba_in_list]['threshold'].mean(),
            'Mínimo': UP_TH_DF[UP_TH_DF['Prueba'] == prueba_in_list]['threshold'].min(),
            'Máximo': UP_TH_DF[UP_TH_DF['Prueba'] == prueba_in_list]['threshold'].max(),
            'StdEst': UP_TH_DF[UP_TH_DF['Prueba'] == prueba_in_list]['threshold'].std(),
            'Mediana': UP_TH_DF[UP_TH_DF['Prueba'] == prueba_in_list]['threshold'].median()})
        up_th_stats_df = pd.concat([up_th_stats_df, new_row_th.to_frame().T], ignore_index=True)
    up_th_stats_df.to_csv(f"Analisis_threshold_diferenciar_golpeos.csv")

    UP_TH_DF.to_csv(f"UP_TH_DF_{row_jugador.prueba.split('_')[0]}_{valores_para_deteccion_updt[0]}_"
                               f"{valores_para_deteccion_updt[1]}_{valores_para_deteccion_updt[2]}_"
                               f"{valores_para_deteccion_updt[3]}.csv")
    pass