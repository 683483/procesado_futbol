import pandas as pd

from segmentacion_datos import *
from PreProcessing import *
from Neural_models import *
from cod_test import *
import plotly.express as px

def identificacion_cambios_direccion():
    # SACAR UNA TANDA PARA PROBAR EL SCRIPT
    # ToDo: sustituir por una entrada de la función
    manager = Paths_manager()
    df_global = cargar_df_global()
    win_size = 25
    overlap = 10
    player_ID = '04'
    num_tanda = 15
    tanda_test  = df_global[(df_global['player_ID']==player_ID)&(df_global['num_tanda']==float(num_tanda))].reset_index()

    # DETECCIÓN MOVIMIENTO
    path_save = r'D:\Documents\_GitLab\procesado_futbol\Datos_jugadores'
    eventos_detectados, eventos_eliminados, valores_para_deteccion_updt = \
        detecta_golpeos(tanda_test,
                        'gyro_y_r',
                        'CambiosDireccion_gyro_y_r',
                        path_save,
                        window=15, ratio=1_000, hyst_up=10, duracion=120)

    inicio = eventos_detectados[0]
    final = eventos_detectados[1]

    # DAR FORMATO VENTANA
    df_ventanas = aplanar_df_datos_desconocidos(tanda_test.iloc[inicio:final], win_size, overlap,
                                               ['gyro_y_l','gyro_z_l','acc_z_l','gyro_y_r','gyro_z_r','acc_z_r'])

    # METER A LA CNN
    # # load json and create model
    json_file = open(r'D:\Documents\_GitLab\procesado_futbol\AjusteHiperparametros\Conv1D\128_neuronas\model_Conv1D_WinSize=25_Overlap=10_filter=32_kernel=3.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = keras.models.model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(r'D:\Documents\_GitLab\procesado_futbol\AjusteHiperparametros\Conv1D\128_neuronas\model_Conv1D_WinSize=25_Overlap=10_filter=32_kernel=3.h5')
    print("Loaded model from disk")

    X_VENTANAS = np.asarray(df_ventanas)
    X_3dim = X_VENTANAS.reshape((X_VENTANAS.shape[0], X_VENTANAS.shape[1], 1))
    predictions = model.predict(X_VENTANAS)
    max_predictions = np.argmax(predictions, axis=1)
    df_ventanas['argmax_predictions'] = max_predictions

    # IDENTIFICAR CONSECUTIVOS
    df_ventanas = find_consecutive_values(df_ventanas, 'argmax_predictions', [0])
    df_ventanas['density'] = check_density(df_ventanas, 'argmax_predictions', 0)
    df_ventanas['consecutive_count'] = 0
    df_ventanas.loc[df_ventanas['consecutive'].ge(6), 'consecutive_count'] = 3

    # CAMBIO A ESCALA ORIGINAL
    lista_inicios = (df_ventanas.index[df_ventanas['consecutive_count'].diff().fillna(0) == 3] * (final - inicio) /
                     df_ventanas.shape[0] + inicio).astype(int).tolist()
    lista_finales = (
                (df_ventanas.index[df_ventanas['consecutive_count'].diff().fillna(0) == -3] - 1) * (final - inicio) /
                df_ventanas.shape[0] + inicio).astype(int).tolist()

    # GRAFICAR RESULTADOS
    fig = px.line(tanda_test.iloc[inicio:final], y=['gyro_y_l', 'gyro_y_r'], markers='.')
    for inicio_rect,final_rect in zip(lista_inicios,lista_finales):
        fig.add_vrect(x0=inicio_rect, x1=final_rect)
    plot(fig)
    pass
    return lista_inicios, lista_finales

# %%
if __name__ == "__main__":
    identificacion_cambios_direccion()
    # SACAR UNA TANDA PARA PROBAR EL SCRIPT
    manager = Paths_manager()
    df_global = cargar_df_global()
    win_size = 25
    overlap = 10
    player_ID = '04'
    num_tanda = 15
    tanda_test = df_global[
        (df_global['player_ID'] == player_ID) & (df_global['num_tanda'] == float(num_tanda))].reset_index()

    # DETECCIÓN MOVIMIENTO
    path_save = r'D:\Documents\_GitLab\procesado_futbol\Datos_jugadores'
    eventos_detectados, eventos_eliminados, valores_para_deteccion_updt = \
        detecta_golpeos(tanda_test,
                        'gyro_y_r',
                        'CambiosDireccion_gyro_y_r',
                        path_save,
                        window=15, ratio=1_000, hyst_up=10, duracion=120)

    inicio = eventos_detectados[0]
    final = eventos_detectados[1]

    pass




# # comparar todas las combinaciones y elegir la mejor
# df_64_neuronas = pd.read_csv(r'D:\Documents\_GitLab\procesado_futbol\AjusteHiperparametros\Conv1D\64_neuronas'
#                              r'\metricas_modelos.csv')
# df_128_neuronas = pd.read_csv(r'D:\Documents\_GitLab\procesado_futbol\AjusteHiperparametros\Conv1D\128_neuronas'
#                               r'\metricas_modelos.csv')
# df_256_neuronas = pd.read_csv(r'D:\Documents\_GitLab\procesado_futbol\AjusteHiperparametros\Conv1D\256_neuronas'
#                               r'\metricas_modelos.csv')
# comparacion_modelos64 = pd.DataFrame(columns=['num_neurons', 'win_size', 'overlap', 'media'])
# comparacion_modelos128 = pd.DataFrame(columns=['num_neurons', 'win_size', 'overlap', 'media'])
# comparacion_modelos256 = pd.DataFrame(columns=['num_neurons', 'win_size', 'overlap', 'media'])
# for index, row in df_64_neuronas.iterrows():
#     new_row = pd.Series({'num_neurons': 64,
#                          'win_size': row['win_size'],
#                          'overlap': row['overlap'],
#                          'media': (row['true0_pred0_tes'] + row['true1_pred1_tes'] + row['true2_pred2_tes']) / 3})
#     comparacion_modelos64 = pd.concat([comparacion_modelos64, new_row.to_frame().T], ignore_index=True)
# for index, row in df_128_neuronas.iterrows():
#     new_row = pd.Series({'num_neurons': 128,
#                          'win_size': row['win_size'],
#                          'overlap': row['overlap'],
#                          'media': (row['true0_pred0_tes'] + row['true1_pred1_tes'] + row['true2_pred2_tes']) / 3})
#     comparacion_modelos128 = pd.concat([comparacion_modelos128, new_row.to_frame().T], ignore_index=True)
# for index, row in df_256_neuronas.iterrows():
#     new_row = pd.Series({'num_neurons': 256,
#                          'win_size': row['win_size'],
#                          'overlap': row['overlap'],
#                          'media': (row['true0_pred0_tes'] + row['true1_pred1_tes'] + row['true2_pred2_tes']) / 3})
#     comparacion_modelos = pd.concat([comparacion_modelos256, new_row.to_frame().T], ignore_index=True)
#
# fig = px.line(comparacion_modelos64, x='overlap', y='media', color='win_size')
# fig.show()
# fig = px.line(comparacion_modelos128, x='overlap', y='media', markers='.', color='win_size')
# fig.show()
# fig = px.line(comparacion_modelos256, x='overlap', y='media', markers='.', color='win_size')
# fig.show()