import math
import os.path
import time
import os
import re

import smart_insole.controller

import pandas as pd
import numpy as np

from smart_insole.utils.geoslab import import_geoslab_data

from .plot_utils import plot_height, plot_trajectory, plot_orientation, plot_distance, plot_raw_data

# Identificadores de Ensayos
FAST_WALK_CLOCKWISE = 'FW_CW'
FAST_WALK_COUNTER_CLOCKWISE = 'FW_CC'
SLOW_WALK_CLOCKWISE = 'SW_CW'
SLOW_WALK_COUNTERCLOCKWISE = 'SW_CC'
ALTERED_WALK_CLOCKWISE = 'AW_CW'
ALTERED_WALK_COUNTERCLOCKWISE = 'AW_CC'
FIVE_TIMES_SIT_TO_STAND_TEST = '5TSTST'

si_controller = smart_insole.controller.Controller()


class WalkTrial:
    trial_name = ''
    serie_name = ''
    capture_system = ''
    user = ''
    trial_data_path = ''
    sample_rate = 0
    motion_data: pd.DataFrame = None
    gait_data: pd.DataFrame = None
    trial_starts = {}
    start_left = None
    start_right = None


# ensayos Idergo
def get_idergo_trial_name(serie):
    serie_names = {
        '_Serie1': FAST_WALK_COUNTER_CLOCKWISE,
        '_Serie2': FAST_WALK_CLOCKWISE,
        '_Serie3': SLOW_WALK_COUNTERCLOCKWISE,
        '_Serie4': SLOW_WALK_CLOCKWISE,
        '_Serie5': FIVE_TIMES_SIT_TO_STAND_TEST,
        '_Serie6': ALTERED_WALK_COUNTERCLOCKWISE,
        '_Serie7': ALTERED_WALK_CLOCKWISE,
    }
    return serie_names.get(serie, None)


# ensayos howlab
def get_howlab_trial_name(serie):
    serie_names = {
        'Ensayo 1 - Marcha rápida - sentido antihorario': FAST_WALK_COUNTER_CLOCKWISE,
        'Ensayo 2 - Marcha rápida - sentido horario': FAST_WALK_CLOCKWISE,
        'Ensayo 3 - Marcha lenta - sentido antihorario': SLOW_WALK_COUNTERCLOCKWISE,
        'Ensayo 4 - Marcha lenta - sentido horario': SLOW_WALK_CLOCKWISE,
        'Ensayo 5 - 5TSTST': FIVE_TIMES_SIT_TO_STAND_TEST,
        'Ensayo 6 - Marcha Alterada UPM': ALTERED_WALK_COUNTERCLOCKWISE,
        '_Serie7': ALTERED_WALK_CLOCKWISE,
    }
    return serie_names.get(serie, None)


def discover_smartinsole_trials(base_path):
    series = next(os.walk(base_path))[1]
    trials = []
    series_trials = {}
    for serie in series:
        serie_path = os.path.join(base_path, serie)
        serie_name = get_howlab_trial_name(serie)
        user_files = os.listdir(serie_path)
        user_files = [uf for uf in user_files if uf.endswith('csv')]
        user_files.sort()
        for user_file in user_files:
            user_id = os.path.splitext(user_file)[0]
            trial_data = WalkTrial()
            trial_data.user = user_id
            trial_data.trial_name = f'{user_id}[SMI]{serie_name}'
            trial_data.capture_system = 'SMI'
            trial_data.trial_data_path = os.path.join(serie_path, user_file)

            trials.append(trial_data)
            if serie_name not in series_trials:
                series_trials[serie_name] = []
            series_trials[serie_name].append(trial_data)

    return trials, series_trials


def discover_idergo_trials_new(base_path, capture_system):
    trial_groups = next(os.walk(base_path))[1]

    trials = []
    series_trials = {}
    for trial_group in trial_groups:
        trial_group_path = os.path.join(base_path, trial_group)
        users = next(os.walk(trial_group_path))[1]
        for user in users:
            user_path = os.path.join(trial_group_path, user)
            user_id = user.split('_')[-1]
            user_id = int(re.sub('[^\d]', '', user_id))
            user_id = f'HW_{user_id:02}'
            series = next(os.walk(user_path))[1]
            for serie in series:
                trial_data = WalkTrial()
                serie_path = os.path.join(user_path, serie)
                data_paths = next(os.walk(serie_path))[1]
                data_path = os.path.join(serie_path, data_paths[0])
                serie_name = get_idergo_trial_name(serie)

                trial_data.capture_system = capture_system
                trial_data.trial_name = f'{user_id}[{capture_system}]{serie_name}'
                trial_data.trial_data_path = data_path
                trial_data.serie_name = serie_name
                trial_data.user = user_id

                trials.append(trial_data)
                if serie_name not in series_trials:
                    series_trials[serie_name] = []
                series_trials[serie_name].append(trial_data)

    return trials, series_trials


def discover_idergo_trials(base_path, capture_system):
    trial_groups = next(os.walk(base_path))[1]

    trials = []
    series_trials = {}
    for trial_group in trial_groups:
        trial_group_path = os.path.join(base_path, trial_group)
        users = next(os.walk(trial_group_path))[1]
        for user in users:
            user_path = os.path.join(trial_group_path, user)
            user_id = user.split('_')[-1]
            user_id = int(re.sub('[^\d]', '', user_id))
            user_id = f'HW_{user_id:02}'
            series = next(os.walk(user_path))[1]
            for serie in series:
                serie_path = os.path.join(user_path, serie)
                data_paths = next(os.walk(serie_path))[1]
                data_path = os.path.join(serie_path, data_paths[0])
                serie_name = get_idergo_trial_name(serie)
                trial_name = f'{user_id}[{capture_system}]{serie_name}'
                trials.append((trial_name, user_id, serie_name, data_path))
                serie_trials = series_trials.get(serie_name, [])
                serie_trials.append((trial_name, user_id, data_path))
                series_trials[serie_name] = serie_trials

    return trials, series_trials


def import_smartinsole_trial(trial: WalkTrial, output_path=None, generate_plots=True, auto_open=False):
    print(f'Importando datos de la prueba {trial.trial_name} desde {trial.trial_data_path}...')
    # datos de movimiento:
    df = pd.read_csv(trial.trial_data_path)
    trial.motion_data, trial.sample_rate = import_geoslab_data(data=df)

    # Localizamos inicio de prueba
    trial = align_trial_start(trial=trial)
    trial = find_trial_start(trial=trial, min_start_time=2, max_start_time=20)
    # trial = truncate_trial(trial, aligned=True)

    # datos de pisadas
    session = smart_insole.controller.Session()
    session.session_data = trial.motion_data
    session.sample_rate = trial.sample_rate
    si_controller.process_data(session)

    trial.gait_data = session.session_footsteps
    print(
        f'Leídos {len(trial.motion_data.index)} registros de movimiento y {len(trial.gait_data.index)} registros de pisadas')

    if output_path is not None:
        os.makedirs(output_path, exist_ok=True)
        trial_name = trial.trial_name.replace(' ', '_')
        motion_file = os.path.join(output_path, f'{trial_name}.motion.parquet')
        trial.motion_data.to_parquet(motion_file)
        gait_file = os.path.join(output_path, f'{trial_name}.gait.parquet')
        trial.gait_data.to_parquet(gait_file)

    if generate_plots:
        # print(f'Generando gráfico de trayectoria...')
        # plot_trajectory(data=trial.motion_data, trial_name=trial.trial_name, out_path=output_path, auto_open=auto_open)
        print(f'Generando gráfico de datos raw...')
        plot_raw_data(data=trial.motion_data, trial_name=trial.trial_name, out_path=output_path,
                      starts={'left': trial.start_left, 'right': trial.start_right},
                      auto_open=auto_open)
        print(f'Generando gráfico de orientación...')
        plot_orientation(data=trial.motion_data, trial_name=trial.trial_name, out_path=output_path,
                         auto_open=auto_open)
        # print(f'Generando gráfico de distancias...')
        # plot_distance(data=trial.motion_data, gait_data=trial.gait_data, trial_name=trial.trial_name, out_path=output_path,
        #               auto_open=auto_open)
        # print(f'Generando gráfico de altura...')
        # plot_height(data=trial.motion_data, gait_data=trial.gait_data, trial_name=trial.trial_name, out_path=output_path,
        #             auto_open=auto_open)

    return trial


def import_idergo_walk_trial(trial: WalkTrial, output_path=None, generate_plots=True, auto_open=False):
    print(f'Importando datos de la prueba {trial.trial_name} desde {trial.trial_data_path}...')
    # datos de movimiento:
    motion_data: pd.DataFrame
    motion_data, _ = get_walk_motion_data(trial.trial_data_path, True)

    # datos de pisadas
    gait_data: pd.DataFrame
    gait_data, _, _ = get_gait_data(trial.trial_data_path)

    trial.motion_data = motion_data
    trial.gait_data = gait_data

    print(f'Leídos {len(motion_data.index)} registros de movimiento y {len(gait_data.index)} registros de pisadas')

    if output_path is not None:
        os.makedirs(output_path, exist_ok=True)
        trial_name = trial.trial_name.replace(' ', '_')
        motion_file = os.path.join(output_path, f'{trial_name}.motion.parquet')
        motion_data.to_parquet(motion_file)
        gait_file = os.path.join(output_path, f'{trial_name}.gait.parquet')
        gait_data.to_parquet(gait_file)

    if generate_plots:
        print(f'Generando gráfico de trayectoria...')
        plot_trajectory(data=motion_data, trial_name=trial.trial_name, out_path=output_path, auto_open=auto_open)
        print(f'Generando gráfico de orientación...')
        plot_orientation(data=motion_data, gait_data=gait_data, trial_name=trial.trial_name, out_path=output_path,
                         auto_open=auto_open)
        print(f'Generando gráfico de distancias...')
        plot_distance(data=motion_data, gait_data=gait_data, trial_name=trial.trial_name, out_path=output_path,
                      auto_open=auto_open)
        print(f'Generando gráfico de altura...')
        plot_height(data=motion_data, gait_data=gait_data, trial_name=trial.trial_name, out_path=output_path,
                    auto_open=auto_open)

    return trial


def import_idergo_trial(trial_name, input_path, output_path=None, generate_plots=True, auto_open=False):
    print(f'Importando datos de la prueba {trial_name} desde {input_path}...')
    # datos de movimiento:
    motion_data: pd.DataFrame
    motion_data, _ = get_walk_motion_data(input_path, True)

    # datos de pisadas
    gait_data: pd.DataFrame
    gait_data, _, _ = get_gait_data(input_path)

    print(f'Leídos {len(motion_data.index)} registros de movimiento y {len(gait_data.index)} registros de pisadas')

    if output_path is not None:
        os.makedirs(output_path, exist_ok=True)
        trial_name = trial_name.replace(' ', '_')
        motion_file = os.path.join(output_path, f'{trial_name}.motion.parquet')
        motion_data.to_parquet(motion_file)
        gait_file = os.path.join(output_path, f'{trial_name}.gait.parquet')
        gait_data.to_parquet(gait_file)

    if generate_plots:
        print(f'Generando gráfico de trayectoria...')
        plot_trajectory(data=motion_data, trial_name=trial_name, out_path=output_path, auto_open=auto_open)
        print(f'Generando gráfico de orientación...')
        plot_orientation(data=motion_data, gait_data=gait_data, trial_name=trial_name, out_path=output_path,
                         auto_open=auto_open)
        print(f'Generando gráfico de distancias...')
        plot_distance(data=motion_data, gait_data=gait_data, trial_name=trial_name, out_path=output_path,
                      auto_open=auto_open)
        print(f'Generando gráfico de altura...')
        plot_height(data=motion_data, gait_data=gait_data, trial_name=trial_name, out_path=output_path,
                    auto_open=auto_open)

    return motion_data, gait_data


def curate_gait_data(gait_data: pd.DataFrame, foot):
    col_0 = gait_data.columns[0]
    stubnames = 'stride '
    data = pd.wide_to_long(gait_data, stubnames=stubnames, i=col_0, j='stride').reset_index()
    data.columns = ['event', 'stride', 'frame']
    data.loc[:, 'event'] = data.loc[:, 'event'].str.replace('GE ', 'T')
    data.loc[:, 'event'] = data.loc[:, 'event'].str.replace(' \[fr\]', '')
    sample_rate = float(col_0)
    data.loc[:, 'absolute_time'] = data.loc[:, 'frame'] / sample_rate
    data['foot'] = foot

    return data


def curate_motion_data(data: pd.DataFrame, foot, use_insole_ref=True):
    p_cols = ['x', 'y', 'z']
    r_cols = ['roll', 'pitch', 'yaw']
    col_names = ['seq_number', 'absolute_time'] + p_cols + r_cols
    data.columns = col_names
    data.loc[:, p_cols] = data.loc[:, p_cols].apply(lambda x: x / 100)  # cm ==> m
    # data.loc[:, r_cols] = data.loc[:,r_cols].apply(np.deg2rad)

    # La orientación del sistema de referencia de la plantilla tiene en cuenta el pie
    if use_insole_ref:
        # El cambio de referencia en la coordenada y no tiene interés para compararlo con los datos de la plantilla
        # data.loc[:, 'y'] = data.loc[:, 'y'] if foot == 'right' else -data.loc[:, 'y']
        data.loc[:, 'roll'] = data.loc[:, 'roll'] if foot == 'right' else -data.loc[:, 'roll']
        data.loc[:, 'pitch'] = -data.loc[:, 'pitch']
        data.loc[:, 'yaw'] = data.loc[:, 'yaw'] if foot == 'left' else -data.loc[:, 'yaw']

    data['time_diff'] = data['absolute_time'].diff()

    data['foot'] = foot
    data['dx'] = data['x'].diff()
    data['dy'] = data['y'].diff()
    data['dz'] = data['z'].diff()

    # velocidades numéricas
    data['speed_x'] = data['dx'] / data['time_diff']
    data['speed_y'] = data['dy'] / data['time_diff']
    data['speed_z'] = data['dz'] / data['time_diff']

    # aceleraciones numéricas
    data['acc_x'] = data['speed_x'].diff() / data['time_diff']
    data['acc_y'] = data['speed_y'].diff() / data['time_diff']
    data['acc_z'] = data['speed_z'].diff() / data['time_diff']

    # la velocidad y acleración en el eje y sí es relevante ponerla en el sistema de referencia de la plantilla para
    # comparar con los datos de las plantillas
    if use_insole_ref and foot == 'left':
        data.loc[:, 'speed_y'] = -data.loc[:, 'speed_y']
        data.loc[:, 'acc_y'] = -data.loc[:, 'acc_y']

    data['distance_diff'] = np.sqrt(np.square(data.dx) + np.square(data.dy) + np.square(data.dz))
    data['distance'] = data['distance_diff'].cumsum()
    data['speed'] = data['distance_diff'] / data['time_diff']
    data['distance_diff_x'] = np.abs(data.dx)
    data['distance_diff_y'] = np.abs(data.dy)
    data['distance_diff_z'] = np.abs(data.dz)
    data['distance_diff_xy'] = np.sqrt(np.square(data.dx) + np.square(data.dy))
    data['distance_diff_xz'] = np.sqrt(np.square(data.dx) + np.square(data.dz))
    data['distance_diff_yz'] = np.sqrt(np.square(data.dy) + np.square(data.dz))
    data['distance_x'] = data['distance_diff_x'].cumsum()
    data['distance_y'] = data['distance_diff_y'].cumsum()
    data['distance_z'] = data['distance_diff_z'].cumsum()
    data['distance_xy'] = data['distance_diff_xy'].cumsum()
    data['distance_xz'] = data['distance_diff_xz'].cumsum()
    data['distance_yz'] = data['distance_diff_yz'].cumsum()
    data['speed_xy'] = data['distance_diff_xy'] / data['time_diff']
    data['speed_xz'] = data['distance_diff_xz'] / data['time_diff']
    data['speed_yz'] = data['distance_diff_yz'] / data['time_diff']
    if use_insole_ref:
        data.loc[:, 'y'] = data.loc[:, 'y'] if foot == 'right' else -data.loc[:, 'y']

    return data


def get_gait_data(sample_path):
    l_gait_file = os.path.join(sample_path, 'GaitEvents_L.csv')
    r_gait_file = os.path.join(sample_path, 'GaitEvents_R.csv')
    if os.path.exists(l_gait_file):
        l_gait_df = pd.read_csv(l_gait_file, header=3, sep=';')
    else:
        return None, None, None
    if os.path.exists(r_gait_file):
        r_gait_df = pd.read_csv(r_gait_file, header=3, sep=';')
    else:
        return None, None, None

    l_data = curate_gait_data(l_gait_df, 'left')
    r_data = curate_gait_data(r_gait_df, 'right')

    data = pd.concat([l_data, r_data])

    return data, l_gait_df, r_gait_df


def get_walk_motion_data(trial_path, quat=True):
    files = os.listdir(trial_path)
    if quat:
        motion_file = next((f for f in files if f.startswith('MovQuat_')), None)
    elif 'Motion.csv' in files:
        motion_file = 'Motion.csv'
    else:
        motion_file = None

    if motion_file:
        motion_file = os.path.join(trial_path, motion_file)
    else:
        return None

    motion_df = pd.read_csv(motion_file, header=2, sep=';')

    # head_vars = ['120', 'Stamp[seg]']
    head_vars = list(motion_df.columns[0:2])
    if quat:
        foot_vars = ['Z', 'X', 'Y', 'Rrz', 'Rrx', 'Rry']
    else:
        foot_vars = ['Z', 'X', 'Y', 'Rz', 'Rx', 'Ry']

    l_data = motion_df.loc[:, head_vars + [f'PieIz.{v}' for v in foot_vars]]
    r_data = motion_df.loc[:, head_vars + [f'PieDr.{v}' for v in foot_vars]]
    l_data = curate_motion_data(l_data, 'left')
    r_data = curate_motion_data(r_data, 'right')

    data = pd.concat([l_data, r_data])

    return data, motion_df


def get_back_motion_data(trial_path, quat=True):
    files = os.listdir(trial_path)
    if quat:
        motion_file = next((f for f in files if f.startswith('MovQuat_')), None)
    elif 'Motion.csv' in files:
        motion_file = 'Motion.csv'
    else:
        motion_file = None

    if motion_file:
        motion_file = os.path.join(trial_path, motion_file)
    else:
        return None

    motion_df = pd.read_csv(motion_file, header=2, sep=';')

    # head_vars = ['120', 'Stamp[seg]']
    head_vars = list(motion_df.columns[0:2])
    if quat:
        back_vars = ['Z', 'X', 'Y', 'Rrz', 'Rrx', 'Rry']
    else:
        back_vars = ['Z', 'X', 'Y', 'Rz', 'Rx', 'Ry']

    l_data = motion_df.loc[:, head_vars + [f'CaderaIz.{v}' for v in back_vars]]
    r_data = motion_df.loc[:, head_vars + [f'CaderaDr.{v}' for v in back_vars]]
    c_data = motion_df.loc[:, head_vars + [f'Pelvis.{v}' for v in back_vars]]
    f_data = motion_df.loc[:, head_vars + [f'Pelvis.{v}' for v in back_vars]]
    l_data = curate_motion_data(l_data, 'left')
    r_data = curate_motion_data(r_data, 'right')
    c_data = curate_motion_data(r_data, 'center')
    f_data = curate_motion_data(r_data, 'front')

    data = pd.concat([l_data, r_data, c_data, f_data])

    return data, motion_df


#### PROCESAMIENTO DATOS SMARTINSOLE

def align_trial_start(trial: WalkTrial):
    """
    La recepción de datos en las plantillas no empieza en el mismo instante, por lo que se hace una corrección en la
    referencia temporal del ensayo comparando los timestamps en los que se reciben los primeros datos.
    :param trial:
    :return:
    """
    data = trial.motion_data

    l_data_id = data['foot'].eq('left')
    r_data_id = data['foot'].eq('right')
    # Tomamos como referencia el valor medio de los primeros 100 registros, para mitigar el efecto de las desviaciones
    # por los retrasos al registrar los mensajes BLE
    l_timestamp = data.loc[l_data_id, 'timestamp'].iloc[0:100].mean()
    r_timestamp = data.loc[r_data_id, 'timestamp'].iloc[0:100].mean()
    lr_diff = l_timestamp - r_timestamp
    # la diferencia se redondea a la resolución del sample_rate
    lr_diff = round(lr_diff * trial.sample_rate) / trial.sample_rate
    # si han empezado a llegar antes los datos de la plantilla izquierda
    if l_timestamp < r_timestamp:
        # corregimos la referencia de tiempo de la plantilla derecha, sumando la diferencia (lr_diff < 0)
        data.loc[r_data_id, 'absolute_time'] = data.loc[r_data_id, 'absolute_time'] - lr_diff
    else:
        # corregimos la referencia de tiempo de la plantilla izquierda restando la referencia (lr_diff < 0)
        data.loc[l_data_id, 'absolute_time'] = data.loc[l_data_id, 'absolute_time'] + lr_diff
    trial.motion_data = data
    return trial


def find_trial_start(trial: WalkTrial, min_start_time=0, max_start_time=0):
    vars = ['acc_x', 'acc_y', 'acc_z', 'gyro_x', 'gyro_y', 'gyro_z']
    trial_data = trial.motion_data
    if min_start_time > 0:
        trial_data = trial_data[trial_data.absolute_time.ge(min_start_time)]
    if max_start_time > 0:
        trial_data = trial_data[trial_data.absolute_time.le(max_start_time)]
    window_size = trial.sample_rate * 2
    window_size = trial.sample_rate / 2
    ratio = 50
    up_q = 0.15
    trial_starts = {}
    for (insole, foot), insole_data in trial_data.groupby(['insole', 'foot']):
        t_starts = []
        for var in vars:
            flat_data_id, low_th, up_th = find_flat_p(insole_data[var], window=window_size, ratio=ratio)
            # flat_data_id, low_th, up_th = find_flat(insole_data[var], window_size=window_size, up_q=up_q)
            t_start = insole_data.loc[flat_data_id, 'absolute_time'].iloc[-1]
            t_starts.append(t_start)
        start = np.median(t_starts)
        trial_starts[insole] = start
        if insole_data['foot'].iloc[0] == 'left':
            trial.start_left = start
        else:
            trial.start_right = start

    trial.trial_starts = trial_starts
    return trial


def truncate_trial(trial: WalkTrial, aligned=False):
    trial_data = trial.motion_data
    if aligned:
        offset = min(trial.start_left, trial.start_right)
        trial_data.loc[:, 'absolute_time'] = trial_data.loc[:, 'absolute_time'] - offset
    else:
        left_id = trial_data.foot.eq('left')
        right_id = trial_data.foot.eq('right')
        trial_data.loc[left_id, 'absolute_time'] = trial_data.loc[left_id, 'absolute_time'] - trial.start_left
        trial_data.loc[right_id, 'absolute_time'] = trial_data.loc[right_id, 'absolute_time'] - trial.start_right

    trial.motion_data = trial_data[trial_data.absolute_time.ge(0)]
    # for insole in trial_data.insole.unique():
    #     insole_id = trial_data.insole.eq(insole)
    #     trial_data.loc[insole_id, 'absolute_time'] = trial_data.loc[insole_id, 'absolute_time'] - trial.trial_starts[
    #         insole]
    #
    # trial.df = trial_data[trial_data.absolute_time.ge(0)]
    return trial


def find_flat(signal: pd.Series, window_size=20, lo_q=0.01, up_q=0.1):
    wd = signal.rolling(window=window_size, min_periods=1)
    wd_std = wd.std()
    low_th = wd_std.quantile(lo_q)
    up_th = wd_std.quantile(up_q)
    flat_data_id = wd_std.le(up_th)
    return flat_data_id, low_th, up_th


def find_flat_d(signal: pd.Series, lo_q=0.01, up_q=0.1):
    signal_abs = signal.abs()
    signal_diff = signal.diff().abs()
    signal_diff_r = signal_diff.rolling(5).std()
    low_th = signal_diff.quantile(lo_q)
    up_th = signal_diff.quantile(up_q)
    flat_data_id = signal_diff.le(up_th)
    return flat_data_id, low_th, up_th


def find_flat_p(signal: pd.Series, window=5, ratio=1_000):
    signal_diff = signal.diff().fillna(0)
    signal_diff_prod = (signal * signal_diff).abs()
    signal_diff_prod_w = signal_diff_prod.rolling(window, center=True, min_periods=1).mean()
    up_th = signal_diff_prod_w.mean() / ratio
    flat_data_id = signal_diff_prod_w.le(up_th)
    return flat_data_id, signal_diff_prod_w, up_th
